<script>
$(document)
		.ready(
				function() {
					const checkCredibility = document
							.getElementById("check-credibility");

					const lookAlike = document.getElementById("look-a-like");

					const lookALikeTitle = document
							.getElementById("look-a-like-account-title");

					const lookALikeGrid = document
							.getElementById("look-a-like-grid");

					const lookAlikeError = document
							.getElementById("look-a-like-error");

					const influencerCredibilityContainer = document
							.getElementById("influencer-credibility");

					const influencerLooklikeResult = document
							.getElementById("lookalike-results");

					var lookAlikeInfluencers;

					// handle the Check Credibility Button click event
					// when this button is clicked we'll call the
					// checkCredibility() function
					checkCredibility.addEventListener("click", getCredibility);
					lookAlike.addEventListener("click", getLookALike);

					function getCredibility() {
						var userName = $('#user-name').val();
						var sucessMessage = document
								.querySelector('div.w-form-done');
						var errorMessage = document
								.querySelector('div.w-form-fail');
						var response = grecaptcha.getResponse();
						var businessEmail = $('#email').val();

						if (businessEmail.includes("gmail")
								|| businessEmail.includes("yahoo")
								|| businessEmail.includes("outlook")
								|| businessEmail.includes("icloud")) {
							sucessMessage.style.display = "none";
							errorMessage.style.display = "block";

							errorMessage.textContent = "Please use a valid business email address";
							return false;
						}

						else if (userName != 'undefined' && userName != ''
								&& response.length != 0) {
							$
									.get(
											{
												url : "https://app.smartfluence.io:9000/get-influencer-data",
												data : {
													influencerHandle : userName,
													email : businessEmail,
													grecaptcha : response,
													platform : 'instagram'
												}
											})
									.done(
											function(data) {
												influencerCredibilityContainer.style.display = "block"
												$('#profile-image')
														.attr(
																"src",'data:image/jpeg;base64,' + data.userProfile.profileImageUrl);
												$('#handle')
														.text(
																data.userProfile.handle);
												$('#user-description')
														.text(
																data.userProfile.description);
												$('#followers')
														.text(
																formatNumber(data.userProfile.followers));
												$('#engagement-rate')
														.text(
																(formatNumber(data.userProfile.engagementRate * 100))
																		+ '%');
												$('#audience-authenticity')
														.text(
																(formatNumber(data.audienceCredibility * 100))
																		+ '%');
												// lookAlikeInfluencers = data.audienceLookalikes;
												lookAlikeInfluencers = data.userProfile.similarUsers;
											})
									.fail(
											function(data) {
												sucessMessage.style.display = "none";
												errorMessage.style.display = "block";
												console.log(data.status);
												console.log(data.responseText);

												if (data.status == 404) {
													errorMessage.textContent = "The Instagram handle you entered appears to be invalid!";
												} else if (data.status == 500) {
													alert("The Instagram handle you entered appears to be invalid!");
												} else if (data.status == 400) {
													errorMessage.textContent = data.responseText;
													alert(data.responseText);
												} else {
													alert("We have encountered an error! Please try again later!");
												}
											})
						}
					}

					$("#email").focusout(function() {
						var businessEmailAddress = $('#email').val();
						if (businessEmailAddress.includes("gmail")
								|| businessEmailAddress.includes("yahoo")
								|| businessEmailAddress.includes("outlook")
								|| businessEmailAddress.includes("icloud")) {
							alert("Please use a valid business email address");
						}
					});

					function getLookALike() {
						if (lookAlikeInfluencers) {
							influencerLooklikeResult.style.display = 'block';
							for (var i = 0; i < 3; i++) {
								var lookAlikeInfluencer = lookAlikeInfluencers[i];

								$('#lookalike-pp-' + (i + 1)).attr("src",'data:image/jpeg;base64,' + lookAlikeInfluencer.profileImageUrl);
								$('#lookalike-handle-' + (i + 1)).text(
										lookAlikeInfluencer.handle);
								$('#lookalike-handle-' + (i + 1)).attr("href",
										lookAlikeInfluencer.url);
								$('#lookalike-followers-' + (i + 1))
										.text(
												formatNumber(lookAlikeInfluencer.followers));
								$('#lookalike-engRate-' + (i + 1))
										.text(
												(formatNumber((lookAlikeInfluencer.engagement / lookAlikeInfluencer.followers) * 100))
														+ '%');
							}
							lookALikeTitle.style.display = "flex";
							lookALikeGrid.style.display = "grid";
							lookAlikeError.style.display = "none";
						} else {
							lookAlikeError.style.display = "flex";
							lookAlikeError.textContent = "No Lookalike Accounts found";
						}
					}

					function formatNumber(val) {
						if (!isNaN(Number(val))) {
							var temp = Number(val);
							var prefix;
							if (temp < 1000) {
								prefix = '';
							} else if (temp < 1000000) {
								prefix = 'K';
							} else if (temp < 1000000000) {
								prefix = 'M';
							} else {
								prefix = 'B';
							}
							switch (prefix) {
							case 'K':
								temp = (temp / 1000);
								break;
							case 'M':
								temp = (temp / 1000000);
								break;
							case 'B':
								temp = (temp / 1000000000);
								break;
							}
							temp = temp.toFixed(2);
							temp = Number(temp).toLocaleString();
							temp += prefix;
							val = temp;
						} else {
							val = 0;
						}
						return val;
					}
				});
</script>