<script>
$(document)
		.ready(
				function() {
					const compareInfluencers = document
							.getElementById("compare-influencers");

					const influencerOverlapContainer = document
							.getElementById("influencer-overlap");

					const influencerOverlapForm = document
						.getElementById("wf-form-Audience-Overlap-Tool");
					
					compareInfluencers.addEventListener("click",
							getOverlapDetails);

					function getOverlapDetails() {
						var baseAccount = $('#base-account').val();
						var comparisonAccount = $('#comparison-account').val();
						var sucessMessage = document
								.querySelector('div.w-form-done');
						var errorMessage = document
								.querySelector('div.w-form-fail');
						var response = grecaptcha.getResponse();
						var businessEmail = $('#email-Id').val();

						if (businessEmail.includes("gmail")
								|| businessEmail.includes("yahoo")
								|| businessEmail.includes("outlook")
								|| businessEmail.includes("icloud")) {
							sucessMessage.style.display = "none";
							errorMessage.style.display = "block";

							errorMessage.textContent = "Please use a valid business email address";
							return false;
						}

						else if (baseAccount != 'undefined' && baseAccount != ''
								&& comparisonAccount != 'undefined'
								&& comparisonAccount != ''
								&& response.length != 0) {
							$
									.get(
											{
												url : "https://app.smartfluence.io:9000/get-influencers-overlap",
												data : {
													baseUser : baseAccount,
													comparisonUser : comparisonAccount,
													email : businessEmail,
													grecaptcha : response,
													platform : 'instagram'
												}
											})
									.done(
											function(data) {
												influencerOverlapForm.style.display = "none"
												influencerOverlapContainer.style.display = "block"

												for (var i = 0; i < data.length; i++) {
													if (data[i].profileName == 'base') {

														$(
																'#total-follower-value')
																.text(
																		formatNumber(data[i].totalFollowers));

														$(
																'#total-unique-followers-value')
																.text(
																		formatNumber(data[i].totalUniqueFollowers));

														$(
																'#overlap-percentage-value')
																.text(
																		(formatNumber(data[i].overlappingPercentage * 100))
																				+ '%');

														$('#base-image')
																.attr(
																		"src",'data:image/jpeg;base64,' + data[i].profileImageUrl);

														var baseUserhandle = data[i].handle;

														if (baseUserhandle != null) {
															$(
																	'#baseUser-handle')
																	.text(
																			data[i].handle);
														} else {
															$(
																	'#baseUser-handle')
																	.text(
																			baseAccount);
														}

														$(
																'#baseUser-description')
																.text(
																		data[i].description);
													} else if (data[i].profileName == 'comparison') {

														$(
																'#comparison-image')
																.attr(
																		"src",'data:image/jpeg;base64,' + data[i].profileImageUrl);

														var baseUserhandle = data[i].handle;

														if (baseUserhandle != null) {
															$(
																	'#comparisonUser-handle')
																	.text(
																			data[i].handle);
														} else {
															$(
																	'#comparisonUser-handle')
																	.text(
																			baseAccount);
														}

														$(
																'#comparisonUser-description')
																.text(
																		data[i].description);
													}
												}
											})
									.fail(
											function(data) {
												sucessMessage.style.display = "none";
												errorMessage.style.display = "block";
												console.log(data.status);
												console.log(data.responseText);

												if (data.status == 404) {
													errorMessage.textContent = "The Instagram handle you entered appears to be invalid!";
												} else if (data.status == 500) {
													alert("The Instagram handle you entered appears to be invalid!");
												} else if (data.status == 302) {
													errorMessage.textContent = data.responseText;
												} else if (data.status == 400) {
													errorMessage.textContent = data.responseText;
													alert(data.responseText);
												} else {
													alert("We have encountered an error! Please try again later!");
												}
											})
						}
					}

					$("#email").focusout(function() {
						var businessEmailAddress = $('#email').val();
						if (businessEmailAddress.includes("gmail")
								|| businessEmailAddress.includes("yahoo")
								|| businessEmailAddress.includes("outlook")
								|| businessEmailAddress.includes("icloud")) {
							alert("Please use a valid business email id");
						}
					});

					function formatNumber(val) {
						if (!isNaN(Number(val))) {
							var temp = Number(val);
							var prefix;
							if (temp < 1000) {
								prefix = '';
							} else if (temp < 1000000) {
								prefix = 'K';
							} else if (temp < 1000000000) {
								prefix = 'M';
							} else {
								prefix = 'B';
							}
							switch (prefix) {
							case 'K':
								temp = (temp / 1000);
								break;
							case 'M':
								temp = (temp / 1000000);
								break;
							case 'B':
								temp = (temp / 1000000000);
								break;
							}
							temp = temp.toFixed(2);
							temp = Number(temp).toLocaleString();
							temp += prefix;
							val = temp;
						} else {
							val = 0;
						}
						return val;
					}
				});
</script>