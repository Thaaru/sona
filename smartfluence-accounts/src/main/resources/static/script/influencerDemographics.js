<script>
	$(document)
		.ready(
				function() {
					const checkDemographics = document
							.getElementById("check-demographics");

					const influencerDemographicsContainer = document
							.getElementById("influencer-demographics");

					checkDemographics
							.addEventListener("click", getDemographics);

					function getDemographics() {
						var userName = $('#user-name').val();
						var sucessMessage = document
								.querySelector('div.w-form-done');
						var errorMessage = document
								.querySelector('div.w-form-fail');
						var response = grecaptcha.getResponse();
						var businessEmail = $('#email').val();

						if (businessEmail.includes("gmail")
								|| businessEmail.includes("yahoo")
								|| businessEmail.includes("outlook")
								|| businessEmail.includes("icloud")) {
							sucessMessage.style.display = "none";
							errorMessage.style.display = "block";

							errorMessage.textContent = "Please use a valid business email address";
							return false;
						}

						else if (userName != 'undefined' && userName != ''
								&& response.length != 0) {
							$
									.get(
											{
												url : "https://app.smartfluence.io:9000/get-influencer-data",
												data : {
													influencerHandle : userName,
													email : businessEmail,
													grecaptcha : response,
													platform : 'instagram'
												}
											})
									.done(
											function(data) {
												influencerDemographicsContainer.style.display = "block"
												$('#profile-image')
														.attr(
																"src",'data:image/jpeg;base64,' + data.userProfile.profileImageUrl);
												$('#handle')
														.text(
																data.userProfile.handle);
												$('#user-description')
														.text(
																data.userProfile.description);
												$('#engagement-rate')
														.text(
																(formatNumber(data.userProfile.engagementRate * 100))
																		+ '%');
												$('#follower-count')
														.text(
																formatNumber(data.userProfile.followers));
												$('#audience-authenticity')
														.text(
																(formatNumber(data.audienceCredibility * 100))
																		+ '%');

												var gender = data.audienceGender;
												for (var i = 0; i < gender.length; i++) {
													switch (gender[i].code) {
													case "MALE":
														$('#gender-male-value')
																.text(
																		(formatNumber(gender[i].weight * 100))
																				+ '%');
														break;
													case "FEMALE":
														$(
																'#gender-female-value')
																.text(
																		(formatNumber(gender[i].weight * 100))
																				+ '%');
														break;
													default:
														// code block
													}
												}

												var age = data.audienceAge;
												for (var i = 0; i < age.length; i++) {
													switch (age[i].code) {
													case "13-17":
														$('#age-value-13-17')
																.text(
																		(formatNumber(age[i].weight * 100))
																				+ '%');
														break;
													case "18-24":
														$('#age-value-18-24')
																.text(
																		(formatNumber(age[i].weight * 100))
																				+ '%');
														break;
													case "25-34":
														$('#age-value-25-34')
																.text(
																		(formatNumber(age[i].weight * 100))
																				+ '%');
														break;
													case "35-44":
														$('#age-value-35-44')
																.text(
																		(formatNumber(age[i].weight * 100))
																				+ '%');
														break;
													case "45-64":
														$('#age-value-45-65')
																.text(
																		(formatNumber(age[i].weight * 100))
																				+ '%');
														break;
													default:
														// code block
													}
												}

												var countries = data.audienceGeo.countries;
												for (var i = 0; i < countries.length; i++) {
													$('#country-name-' + i)
															.text(
																	countries[i].name);

													$('#country-value-' + i)
															.text(
																	(formatNumber(countries[i].weight * 100))
																			+ '%');
												}

												var states = data.audienceGeo.states;
												for (var i = 0; i < states.length; i++) {
													$('#state-name-' + i).text(
															states[i].name);

													$('#state-value-' + i)
															.text(
																	(formatNumber(states[i].weight * 100))
																			+ '%');
												}

												var interests = data.audienceInterests;
												for (var i = 0; i < interests.length; i++) {
													$('#interest-name-' + i)
															.text(
																	interests[i].name);

													$('#interest-value-' + i)
															.text(
																	(formatNumber(interests[i].weight * 100))
																			+ '%');
												}
											})
									.fail(
											function(data) {
												sucessMessage.style.display = "none";
												errorMessage.style.display = "block";
												console.log(data.status);
												console.log(data.responseText);

												if (data.status == 404) {
													errorMessage.textContent = "The Instagram handle you entered appears to be invalid!";
												} else if (data.status == 500) {
													alert("The Instagram handle you entered appears to be invalid!");
												} else if (data.status == 400) {
													errorMessage.textContent = data.responseText;
													alert(data.responseText);
												} else {
													alert("We have encountered an error! Please try again later!");
												}
											})
						}
					}

					$("#email").focusout(function() {
						var businessEmailAddress = $('#email').val();
						if (businessEmailAddress.includes("gmail")
								|| businessEmailAddress.includes("yahoo")
								|| businessEmailAddress.includes("outlook")
								|| businessEmailAddress.includes("icloud")) {
							alert("Please use a valid business email id");
						}
					});

					function formatNumber(val) {
						if (!isNaN(Number(val))) {
							var temp = Number(val);
							var prefix;
							if (temp < 1000) {
								prefix = '';
							} else if (temp < 1000000) {
								prefix = 'K';
							} else if (temp < 1000000000) {
								prefix = 'M';
							} else {
								prefix = 'B';
							}
							switch (prefix) {
							case 'K':
								temp = (temp / 1000);
								break;
							case 'M':
								temp = (temp / 1000000);
								break;
							case 'B':
								temp = (temp / 1000000000);
								break;
							}
							temp = temp.toFixed(2);
							temp = Number(temp).toLocaleString();
							temp += prefix;
							val = temp;
						} else {
							val = 0;
						}
						return val;
					}
				});
</script>