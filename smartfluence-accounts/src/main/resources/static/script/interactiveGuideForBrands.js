<script>
	$(document)
		.ready(
				function() {
					const searchLookalike = document
							.getElementById("search-lookalike");

					const checkCredibility = document
							.getElementById("check-credibility");

					const influencerLookalikeContainer = document
							.getElementById("influencer-lookalikes");

					const influencerCredibilityContainer = document
							.getElementById("influencer-credibility");

					const influencerProfileOne = document
							.getElementById("influencer-profile-0");

					const influencerProfileTwo = document
							.getElementById("influencer-profile-1");

					const influencerProfileThree = document
							.getElementById("influencer-profile-2");

					searchLookalike.addEventListener("click", getLookalike);
					checkCredibility.addEventListener("click", getCredibility);

					function getLookalike() {
						var lookAlikeUserName = $('#lookalike-user-name').val();
						var audienceIndustry = $('#audience_industry').val();
						var sucessMessage = document
								.querySelector('div.w-form-done');
						var errorMessage = document
								.querySelector('div.w-form-fail');
						var response = grecaptcha.getResponse();
						var audienceFollowersValue = $(
								'#audience_follower_count').val();
						var audienceFollowersMin = audienceFollowersValue
								.substring(0, audienceFollowersValue
										.lastIndexOf('-'));
						var audienceFollowersMax = audienceFollowersValue
								.substring(audienceFollowersValue
										.lastIndexOf('-') + 1);
						var businessEmail = $('#email').val();

						if (businessEmail.includes("gmail")
								|| businessEmail.includes("yahoo")
								|| businessEmail.includes("outlook")
								|| businessEmail.includes("icloud")) {
							sucessMessage.style.display = "none";
							errorMessage.style.display = "block";

							errorMessage.textContent = "Please use a valid business email address";
							return false;
						}

						else if (lookAlikeUserName != 'undefined' && lookAlikeUserName != ''
								&& audienceIndustry != 'undefined'
								&& audienceIndustry != ''
								&& response.length != 0) {
							$
									.get(
											{
												url : "https://app.smartfluence.io:9000/get-influencer-lookalike",
												data : {
													comparisonProfile : lookAlikeUserName,
													industry : audienceIndustry,
													followersMin : audienceFollowersMin,
													followersMax : audienceFollowersMax,
													email : businessEmail,
													grecaptcha : response,
													platform : 'instagram'
												}
											})
									.done(
											function(data) {
												if (data.length == 0) {
													sucessMessage.style.display = "none";
													errorMessage.style.display = "block";
													errorMessage.textContent = "No result found for given criteria! Refresh and try a different search!";
												} else {
													influencerLookalikeContainer.style.display = "block"

													for (var i = 0; i < data.length; i++) {
														if (i == 0) {
															influencerProfileOne.style.display = "block"
														} else if (i == 1) {
															influencerProfileTwo.style.display = "block"
														} else if (i == 2) {
															influencerProfileThree.style.display = "block"
														}

														$(
																'#lookalike-image-'
																		+ i)
																.attr(
																		"src",'data:image/jpeg;base64,' + data[i].profileImageUrl);

														$(
																'#influencer-handle-'
																		+ i)
																.text(
																		data[i].influencerHandle);

														$(
																'#influencer-handle-'
																		+ i)
																.attr(
																		"href",
																		data[i].url);

														$(
																'#influencer-followers-'
																		+ i)
																.text(
																		formatNumber(data[i].followers));

														$(
																'#influencer-engagement-'
																		+ i)
																.text(
																		(formatNumber(data[i].engagementScore * 100))
																				+ '%');
													}
												}
											})
									.fail(
											function(data) {
												sucessMessage.style.display = "none";
												errorMessage.style.display = "block";
												console.log(data.status);
												console.log(data.responseText);

												if (data.status == 404) {
													errorMessage.textContent = "The Instagram handle you entered appears to be invalid!";
												} else if (data.status == 500) {
													alert("The Instagram handle you entered appears to be invalid!");
												} else if (data.status == 400) {
													errorMessage.textContent = data.responseText;
													alert(data.responseText);
												} else {
													alert("We have encountered an error! Please try again later!");
												}
											})
						}
					}

					function getCredibility() {
						var credibilityUserName = $('#credibility-user-name').val();
						var sucessMessage = document
								.querySelector('div.w-form-done');
						var errorMessage = document
								.querySelector('div.w-form-fail');
						var response = grecaptcha.getResponse();

						if (credibilityUserName != 'undefined' && credibilityUserName != ''
								&& response.length != 0) {
							$
									.get(
											{
												url : "https://app.smartfluence.io:9000/get-influencer-data",
												data : {
													influencerHandle : credibilityUserName,
													email : businessEmail,
													grecaptcha : response,
													platform : 'instagram'
												}
											})
									.done(
											function(data) {
												influencerCredibilityContainer.style.display = "block"
												$('#profile-image')
														.attr(
																"src",'data:image/jpeg;base64,' + data.userProfile.profileImageUrl);
												$('#handle')
														.text(
																data.userProfile.handle);
												$('#user-description')
														.text(
																data.userProfile.description);
												$('#followers')
														.text(
																formatNumber(data.userProfile.followers));
												$('#engagement-rate')
														.text(
																(formatNumber(data.userProfile.engagementRate * 100))
																		+ '%');
												$('#audience-authenticity')
														.text(
																(formatNumber(data.audienceCredibility * 100))
																		+ '%');
											})
									.fail(
											function(data) {
												sucessMessage.style.display = "none";
												errorMessage.style.display = "block";
												console.log(data.status);
												console.log(data.responseText);

												if (data.status == 404) {
													errorMessage.textContent = "The Instagram handle you entered appears to be invalid!";
												} else if (data.status == 500) {
													alert("The Instagram handle you entered appears to be invalid!");
												} else if (data.status == 400) {
													errorMessage.textContent = data.responseText;
													alert(data.responseText);
												} else {
													alert("We have encountered an error! Please try again later!");
												}
											})
						}
					}

					$("#email").focusout(function() {
						var businessEmailAddress = $('#email').val();
						if (businessEmailAddress.includes("gmail")
						|| businessEmailAddress.includes("outlook")
						|| businessEmailAddress.includes("yahoo")
						|| businessEmailAddress.includes("icloud")) {
							alert("Please use a valid business email address");
						}
					});

					function formatNumber(val) {
						if (!isNaN(Number(val))) {
							var temp = Number(val);
							var prefix;
							if (temp < 1000) {
								prefix = '';
							} else if (temp < 1000000) {
								prefix = 'K';
							} else if (temp < 1000000000) {
								prefix = 'M';
							} else {
								prefix = 'B';
							}
							switch (prefix) {
							case 'K':
								temp = (temp / 1000);
								break;
							case 'M':
								temp = (temp / 1000000);
								break;
							case 'B':
								temp = (temp / 1000000000);
								break;
							}
							temp = temp.toFixed(2);
							temp = Number(temp).toLocaleString();
							temp += prefix;
							val = temp;
						} else {
							val = 0;
						}
						return val;
					}
				});
</script>