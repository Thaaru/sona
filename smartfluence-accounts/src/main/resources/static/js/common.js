var EMAIL_REGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

function log(msg) {
    console.log(msg);
}

$.ajaxSetup({
	beforeSend: function (xhr)
    {
       //xhr.setRequestHeader("Accept-Language",navigator.language);
       
    },
    error: function(jqxhr, settings, thrownError) {
        if (jqxhr.status == 401) {
            setTimeout(function() {
                window.location.href = window.location.origin;
            }, 1000)
        }
    }
});


$('.password-toggle').on('click', function() {
    $(this).children().toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("data-toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

function formSubmit(e){
	  e.preventDefault();
	    var isValid = true;
	    $(this).find('input,select,textarea').each(function() {
	        if ($(this).is('[required]:invalid')) {
	            isValid = false;
	        }
	    });
	    if (!isValid) {
	        $(this).addClass('was-validated');
	    }
	    if (isValid && formCallBack) {
	        formCallBack(this);
	    }
	    return false;
	
}
$('form.needs-validation').bind('submit',formSubmit);

function resetForms() {
    $('form').each(function() {
        this.reset();
        $(this).removeClass('was-validated');
    })
}

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("active");

    var imgElement = document.getElementById("logo");
    var src = "";
    src = imgElement.src.replace(/^.*[\\\/]/, '');
    imgElement.src = src === 'new-logo-pink.png' ? '/img/new_Icon.png' : '/img/new-logo-pink.png';

    if (src === 'new-logo-pink.png') {
		$('#logo').css('width','45px');
		$('#logo').css('height','45px');
	} else {
		$('#logo').css('width','200px');
		$('#logo').css('height','30px');
		$('#logo').css('padding-right','3.5px');
	}
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "2000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

var SUCCESS = toastr["success"];

var ERROR = toastr["error"];

var WARNING = toastr["warning"];

var INFO = toastr["info"];

String.prototype.contains = function(char) {
    return this.indexOf(char) != -1
};

var currentUrl = new URL(window.location.href);

function getParam(param) {
    if (typeof param == 'string')
        return currentUrl.searchParams.get(param);
    return null;
}

function hrefLinker() {
    var url = currentUrl;
    if (url.hash && url.hash.length > 0)
        $('[href="' + url.hash + '"]').click();
}

$(document).ready(function() {
    hrefLinker();
    customSlider();
});

function customSlider() {
    var size = 30;
    $('.range-field input[type="range"]').each(function() {
    	var suffix=$(this).attr('data-suffix')==undefined?'':$(this).attr('data-suffix');
        $(this).parent().prev('span').text(numberFunction($(this).attr('min')) + suffix);
        $(this).parent().next('span').text(numberFunction($(this).attr('max')) +suffix);
        var rightVal = 'calc(' + ((this.value / this.max) * 100) + '% - 12px)';
        $(this).parent().children('.thumb').css('left', rightVal);
        $(this).parent().find('.value').text(numberFunction(this.value) + suffix);
    });
    $('.range-field input[type="range"]').mousedown(function() {
    	var suffix=$(this).attr('data-suffix')==undefined?'':$(this).attr('data-suffix');
    	var rightVal = 'calc(' + ((this.value / this.max) * 100) + '% - 12px)';
        $(this).parent().children('.thumb').css('left', rightVal);
        $(this).parent().find('.value').text(numberFunction(this.value) + suffix);
        $(this).parent().children('.thumb').css({
            top: -size + 3,
            width: size,
            height: size
        });
    }).on('input', function() {
    	var suffix=$(this).attr('data-suffix')==undefined?'':$(this).attr('data-suffix');
        var rightVal = 'calc(' + ((this.value / this.max) * 100) + '% - 12px)';
        $(this).parent().children('.thumb').css('left', rightVal);
        $(this).parent().find('.value').text(numberFunction(this.value)+ suffix);
    });


    $('.range-field input[type="range"]').mouseout(function() {
        $(this).parent().children('.thumb').css({
        	top: 0,
            width: 0,
            height: 0
        })
    });
}

function numberFunction(val) {
    var temp = Number(val);
    var prefix;
    if (temp < 1000) {
        prefix = '';
    } else if (temp < 1000000) {
        prefix = 'K';
    } else if (temp < 1000000000) {
        prefix = 'M';
    } else {
        prefix = 'B';
    }
    switch (prefix) {
        case 'K':
            temp = (temp / 1000).toFixed(0);
            break;
        case 'M':
            temp = (temp / 1000000).toFixed(0);
            break;
        case 'B':
            temp = (temp / 1000000000).toFixed(0);
            break;
    }
    temp += prefix;
    val = temp;
    return val;
}