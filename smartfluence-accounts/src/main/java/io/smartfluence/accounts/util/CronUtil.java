package io.smartfluence.accounts.util;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.primary.SubscriptionMasterKey;
import io.smartfluence.cassandra.repository.SubscriptionMasterRepo;
import io.smartfluence.cassandra.repository.brand.BrandCreditsRepo;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandSubscriptionsRepo;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.APIServiceEmailValidationRepo;
import io.smartfluence.mysql.repository.APIServiceIPValidationRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Configuration
@EnableScheduling
public class CronUtil {

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private BrandSubscriptionsRepo brandSubscriptionsRepo;

	@Autowired
	private BrandCreditsRepo brandCreditsRepo;

	@Autowired
	private SubscriptionMasterRepo subscriptionMasterRepo;

	@Autowired
	private APIServiceEmailValidationRepo apiServiceEmailValidationRepo;

	@Autowired
	private APIServiceIPValidationRepo apiServiceIPValidationRepo;

	@Value("${user.renew.subscriptions}")
	private String renewSubscriptions;

	@Scheduled(cron = "${user.accounts.cron}")
	public void dailyMidNightTask() {
		List<String> renewSubscriptionsList = new ArrayList<String>(Arrays.asList(renewSubscriptions.split(",")));
		brandDetailsRepo.findAll().forEach(e -> {
			beginInactiveTransaction(e, renewSubscriptionsList);
		});

		apiServiceEmailValidationRepo.updateDailyCount();
		apiServiceIPValidationRepo.updateDailyCount();
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW)
	private void beginInactiveTransaction(BrandDetails brandDetails, List<String> renewSubscriptionsList) {
		try {
			if ((UserStatus.APPROVED.equals(brandDetails.getKey().getUserStatus())
					|| UserStatus.INACTIVE.equals(brandDetails.getKey().getUserStatus()))
					&& brandDetails.getValidUpto().toInstant().isBefore(Instant.now())) {

				if (renewSubscriptionsList.contains(brandDetails.getSubscriptionType().toString()))
					checkSubscriptionForRenew(brandDetails);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private void checkSubscriptionForRenew(BrandDetails brandDetails) throws CloneNotSupportedException {
		boolean isRenewed = false;
		boolean resetCredits = true;
		Set<BrandSubscriptions> subscriptionsSet = brandSubscriptionsRepo.findAllByKeyBrandIdAndKeyActiveIsTrue(brandDetails.getKey().getUserId());

		for (BrandSubscriptions brandSubscriptions : subscriptionsSet) {
			if (!brandSubscriptions.isResetCredit()) {
				resetCredits = false;
				continue;
			}
			if (brandSubscriptions.isRenewable()) {
				try {
					updateSubscriptions(brandDetails, brandSubscriptions);
					isRenewed = true;
				} catch (Exception e) {
					log.error(e);
				}
			}
		}

		if (resetCredits) {
			UserStatus userStatus = isRenewed ? UserStatus.APPROVED : UserStatus.INACTIVE;

			changeBrandDetailStatus(brandDetails, userStatus);
			changeUserAccountStatus(brandDetails, userStatus);
		}
	}

	public void updateSubscriptions(BrandDetails brandDetails, BrandSubscriptions brandSubscriptions) throws CloneNotSupportedException {
		makeOldSubscriptionInactive(brandSubscriptions);
		BrandSubscriptions newSubscription = createRenewedSubscription(brandSubscriptions);
		updateValidationDate(brandDetails, newSubscription);
	}

	private void makeOldSubscriptionInactive(BrandSubscriptions brandSubscriptions) throws CloneNotSupportedException {
		BrandSubscriptions subscriptionsUpdate = (BrandSubscriptions) brandSubscriptions.copy();
		subscriptionsUpdate.setModifiedAt(new Date());
		brandSubscriptionsRepo.delete(brandSubscriptions);
		subscriptionsUpdate.getKey().setActive(false);
		brandSubscriptionsRepo.save(subscriptionsUpdate);
	}

	private BrandSubscriptions createRenewedSubscription(BrandSubscriptions brandSubscriptions) throws CloneNotSupportedException {
		Date newdate = new Date();
		BrandSubscriptions newSubscription = (BrandSubscriptions) brandSubscriptions.copy();
		Optional<SubscriptionMaster> subscriptionMaster = subscriptionMasterRepo.findById(SubscriptionMasterKey.builder()
				.subscriptionType(newSubscription.getSubscriptionType()).subscriptionId(newSubscription.getKey().getSubscriptionId()).build());

		newSubscription.getKey().setTransactionId(UUID.randomUUID());
		newSubscription.setSubscriptionDate(newdate);
		newSubscription.setCredits(subscriptionMaster.get().getCredits());
		newSubscription.setRenewable(subscriptionMaster.get().isRenewable());
		newSubscription.setResetCredit(subscriptionMaster.get().isResetCredit());
		newSubscription.setSubscriptionDate(newdate);
		newSubscription.setSubscriptionName(subscriptionMaster.get().getSubscriptionName());
		newSubscription.setValidity(subscriptionMaster.get().getValidity());
		newSubscription.setValidityType(subscriptionMaster.get().getValidityType());
		newSubscription.setCreatedAt(newdate);
		newSubscription.setModifiedAt(newdate);

		brandSubscriptionsRepo.save(newSubscription);
		Optional<BrandCredits> optionalBrandCredits = brandCreditsRepo.findById(brandSubscriptions.getKey().getBrandId());

		if(optionalBrandCredits.isPresent()) {
			BrandCredits brandCredits = optionalBrandCredits.get();
			brandCredits.setModifiedAt(new Date());
			brandCredits.setValidUpto(Date.from(getLastDateOfSubscription(newSubscription).toInstant()));
			brandCredits.setCredits(newSubscription.getCredits());

			brandCreditsRepo.save(brandCredits);
		}
		return newSubscription;
	}

	private void updateValidationDate(BrandDetails brandDetails, BrandSubscriptions newSubscription) {
		Date newValidationDate = Date.from(getLastDateOfSubscription(newSubscription).toInstant());
		brandDetails.setValidUpto(newValidationDate);
		brandDetails.setModifiedAt(new Date());
		brandDetailsRepo.save(brandDetails);
	}

	private void changeBrandDetailStatus(BrandDetails brandDetails, UserStatus userStatus)
			throws CloneNotSupportedException {
		if (!brandDetails.getKey().getUserStatus().equals(userStatus)) {
			BrandDetails brandDetailsUpdate = (BrandDetails) brandDetails.copy();
			brandDetailsUpdate.getKey().setUserStatus(userStatus);
			brandDetailsUpdate.setModifiedAt(new Date());
			brandDetailsRepo.delete(brandDetails);
			brandDetailsRepo.save(brandDetailsUpdate);
		}
	}

	private void changeUserAccountStatus(BrandDetails brandDetails, UserStatus userStatus) {
		Optional<UserAccounts> userAccountsOpt = userAccountsRepo
				.findById(brandDetails.getKey().getUserId().toString());
		if (userAccountsOpt.isPresent() && !userAccountsOpt.get().getUserStatus().equals(userStatus)) {
			UserAccounts userAccounts = userAccountsOpt.get();
			userAccounts.setUserStatus(userStatus);
			userAccounts.setModifiedAt(new Date());
			userAccountsRepo.save(userAccounts);
		}
	}

	private ZonedDateTime getLastDateOfSubscription(BrandSubscriptions subscriptions) {
		Date date = subscriptions.getSubscriptionDate();
		ZonedDateTime zoneDateTime = date.toInstant().atZone(ZoneId.systemDefault()).withHour(0).withMinute(0)
				.withSecond(0).withNano(0);
		switch (subscriptions.getValidityType()) {
		case DAY:
			zoneDateTime = zoneDateTime.plusDays(subscriptions.getValidity());
			break;
		case MONTH:
			zoneDateTime = zoneDateTime.plusMonths(subscriptions.getValidity());
			break;
		case YEAR:
			zoneDateTime = zoneDateTime.plusYears(subscriptions.getValidity());
			break;
		case HOUR:
			zoneDateTime = zoneDateTime.plusHours(subscriptions.getValidity());
			break;
		case MINUTE:
			zoneDateTime = zoneDateTime.plusMinutes(subscriptions.getValidity());
			break;
		default:
			break;
		}
		return zoneDateTime;
	}
}