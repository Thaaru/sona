package io.smartfluence.accounts.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.smartfluence.beans.service.SharedBeanService;
import io.smartfluence.constants.UserType;
import io.smartfluence.modal.validation.RegistrationModal;

@Controller
public class HomeView {

	private static final String REGISTRATION_MODAL = "registrationModal";

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@Autowired
	private SharedBeanService sharedBeanService;

	private RegistrationModal registrationModalBean = new RegistrationModal();

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@GetMapping("/")
	public String home() {
		return "redirect:/login";
	}

	@GetMapping("/login")
	public String signIn(Model model, HttpServletRequest request, HttpServletResponse response, Principal principal,
			HttpSession session) throws IOException {

		Cookie authCookie = null;
		if (request.getCookies() != null) {
			List<Cookie> cookies = Arrays.asList(request.getCookies());
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("SMARTFLUENCE")) {
					authCookie = cookie;
					break;
				}
			}
		}
		if (principal != null) {
			if (authCookie != null) {
				authCookie.setValue(null);
				authCookie.setMaxAge(0);
				response.addCookie(authCookie);
			}

			redirectStrategy.sendRedirect(request, response,
					ServletUriComponentsBuilder.fromCurrentContextPath().host(eurekaInstanceConfigBean.getHostname())
							.port(-1).path("/login").build(true).toUri().toString());
		}

		if (!model.containsAttribute(REGISTRATION_MODAL)) {
			registrationModalBean.setUserType(UserType.BRAND);
			model.addAttribute(REGISTRATION_MODAL, registrationModalBean);
		}
		model.addAttribute("isFree", "false");
		model.addAttribute("subscriptionId", "");
		model.addAttribute("signInActive", "active show");
		model.addAttribute("industries", sharedBeanService.getIndustryList().getBody());
		return "Home";
	}

	@GetMapping("sign-up/{subscriptionId}")
	public String signUp(Model model, @PathVariable String subscriptionId) {
		if (!model.containsAttribute(REGISTRATION_MODAL)) {
			registrationModalBean.setUserType(UserType.BRAND);
			model.addAttribute(REGISTRATION_MODAL, registrationModalBean);
		}
		model.addAttribute("isFree", "true");
		model.addAttribute("subscriptionId", subscriptionId);
		model.addAttribute("industries", sharedBeanService.getIndustryList().getBody());

		return "Home";
	}

	@GetMapping("register-successful")
	public String registerSuccessful() {
		return "RegisterSuccessful";
	}

	@GetMapping("verify-email")
	public String verifyEmailView() {
		return "VerifyEmail";
	}

	@GetMapping("forgot-password-successful")
	public String forgotPasswordSuccessful() {
		return "ForgotPasswordSuccessful";
	}

	@GetMapping("reset-password")
	public String resetPasswordView(Model model) {
		return "ResetPassword";
	}

	@GetMapping("reset-password-successfully")
	public String resetPasswordSuccessfully(Model model) {
		return "ResetPasswordSuccessful";
	}

	@GetMapping("accept-offer")
	public String promoteCampaignInviteView() {
		return "PromoteCampaignInvite";
	}
}