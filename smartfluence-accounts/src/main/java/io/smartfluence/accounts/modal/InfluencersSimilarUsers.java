package io.smartfluence.accounts.modal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencersSimilarUsers {

	private String profileImage;

	private byte[] profileImageUrl;

	private String handle;

	private Double followers;

	private String fullName;

	private String url;

	private Double engagement;
}