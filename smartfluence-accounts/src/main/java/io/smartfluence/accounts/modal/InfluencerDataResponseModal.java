package io.smartfluence.accounts.modal;

import java.util.ArrayList;
import java.util.List;

import io.smartfluence.modal.social.report.AudienceGeo;
import io.smartfluence.modal.social.report.CodeAndWeight;
import io.smartfluence.modal.social.report.NameAndWeight;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.Builder.Default;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InfluencerDataResponseModal {

	private InfluencersUserProfile userProfile;

	private Double audienceCredibility;

	@Default
	private List<CodeAndWeight> audienceGender = new ArrayList<>();

	@Default
	private List<CodeAndWeight> audienceAge = new ArrayList<>();

	@Default
	private List<NameAndWeight> audienceInterests = new ArrayList<>();

	@Default
	private AudienceGeo audienceGeo = new AudienceGeo();

	@Default
	private List<InfluencersLookalikes> audienceLookalikes = new ArrayList<>();
}