package io.smartfluence.accounts.modal;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class InfluencersUserProfile {

	private String platformType;

	private String handle;

	private String url;

	private String profileImage;

	private byte[] profileImageUrl;

	private String fullName;

	private String description;

	private Double followers;

	private Double engagementRate;

	@Default
	private List<InfluencersSimilarUsers> similarUsers = new ArrayList<>();
}