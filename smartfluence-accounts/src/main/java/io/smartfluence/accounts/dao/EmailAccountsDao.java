package io.smartfluence.accounts.dao;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import io.smartfluence.cassandra.entities.InfluencerDetails;
import io.smartfluence.cassandra.entities.PromoteInfluencersData;
import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.primary.SubscriptionMasterKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.UserAccounts;

public interface EmailAccountsDao {

	UserAccounts saveUserAccounts(UserAccounts userAccounts);

	UserAccounts getUserAccountsByEmail(String email);

	Optional<UserAccounts> getUserAccountsById(String userId);

	UserAccounts getUserAccountsByVC(String verificationCode);

	void saveBrandDetails(BrandDetails brandDetails);

	void deleteBrandDetails(BrandDetails brandDetails);

	Optional<BrandDetails> getBrandDetailsById(UserDetailsKey key);

	Optional<BrandDetails> getBrandDetailsByUserStatusInAndUserId(Set<UserStatus> userStatus, UUID userId);

	void saveInfluencerDetails(InfluencerDetails influencerDetails);

	void deleteInfluencerDetails(InfluencerDetails influencerDetails);

	Optional<InfluencerDetails> getInfluencerDetailsById(UserDetailsKey key);

	void saveBrandSubscriptions(BrandSubscriptions brandSubscriptions);

	void deleteBrandSubscriptions(BrandSubscriptions brandSubscriptions);

	Optional<BrandSubscriptions> getBrandSubscriptionsByBrandIdAndActiveIsTrue(UUID brandId);

	void saveBrandCredits(BrandCredits brandCredits);

	Optional<SubscriptionMaster> getSubscriptionMaster(SubscriptionMasterKey key);

	Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignIdAndBrandId(String campaignId, String brandId);

	Optional<PromoteInfluencersData> getPromoteInfluencersDataByCampaignIdAndInfluencerDeepSocialId(UUID campaignId, String influencerDeepSocialId);

	void savePromoteInfluencersData(PromoteInfluencersData promoteInfluencersData);

	Optional<MailTemplateMaster> getMailTemplateMasterByTemplateName(String name);
}