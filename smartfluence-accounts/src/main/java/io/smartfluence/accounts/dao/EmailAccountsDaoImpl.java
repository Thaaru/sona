package io.smartfluence.accounts.dao;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.InfluencerDetails;
import io.smartfluence.cassandra.entities.PromoteInfluencersData;
import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.primary.SubscriptionMasterKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandCreditsRepo;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.cassandra.repository.brand.BrandSubscriptionsRepo;
import io.smartfluence.cassandra.repository.InfluencerDetailsRepo;
import io.smartfluence.cassandra.repository.PromoteInfluencersDataRepo;
import io.smartfluence.cassandra.repository.SubscriptionMasterRepo;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.BrandCampaignDetailsMysqlRepo;
import io.smartfluence.mysql.repository.MailTemplateMasterRepo;
import io.smartfluence.mysql.repository.UserAccountsRepo;

@Repository
public class EmailAccountsDaoImpl implements EmailAccountsDao {

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	@Autowired
	private InfluencerDetailsRepo influencerDetailsRepo;

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@Autowired
	private SubscriptionMasterRepo subscriptionMasterRepo;

	@Autowired
	private BrandCreditsRepo brandCreditsRepo;

	@Autowired
	private BrandSubscriptionsRepo brandSubscriptionsRepo;

	@Autowired
	private BrandCampaignDetailsMysqlRepo brandCampaignDetailsMysqlRepo;

	@Autowired
	private PromoteInfluencersDataRepo promoteInfluencersDataRepo;

	@Autowired
	private MailTemplateMasterRepo mailTemplateMasterRepo;

	@Override
	public UserAccounts saveUserAccounts(UserAccounts userAccounts) {
		return userAccountsRepo.save(userAccounts);
	}

	@Override
	public UserAccounts getUserAccountsByEmail(String email) {
		return userAccountsRepo.findByEmail(email);
	}

	@Override
	public Optional<UserAccounts> getUserAccountsById(String userId) {
		return userAccountsRepo.findById(userId);
	}

	@Override
	public UserAccounts getUserAccountsByVC(String verificationCode) {
		return userAccountsRepo.findByVC(verificationCode);
	}

	@Override
	public void saveBrandDetails(BrandDetails brandDetails) {
		brandDetailsRepo.save(brandDetails);
	}

	@Override
	public void deleteBrandDetails(BrandDetails brandDetails) {
		brandDetailsRepo.delete(brandDetails);
	}

	@Override
	public Optional<BrandDetails> getBrandDetailsById(UserDetailsKey key) {
		return brandDetailsRepo.findById(key);
	}

	@Override
	public Optional<BrandDetails> getBrandDetailsByUserStatusInAndUserId(Set<UserStatus> userStatus, UUID userId) {
		return brandDetailsRepo.findByKeyUserStatusInAndKeyUserId(userStatus, userId);
	}

	@Override
	public void saveInfluencerDetails(InfluencerDetails influencerDetails) {
		influencerDetailsRepo.save(influencerDetails);
	}

	@Override
	public void deleteInfluencerDetails(InfluencerDetails influencerDetails) {
		influencerDetailsRepo.delete(influencerDetails);
	}

	@Override
	public Optional<InfluencerDetails> getInfluencerDetailsById(UserDetailsKey key) {
		return influencerDetailsRepo.findById(key);
	}

	@Override
	public void saveBrandSubscriptions(BrandSubscriptions brandSubscriptions) {
		brandSubscriptionsRepo.save(brandSubscriptions);
	}

	@Override
	public void deleteBrandSubscriptions(BrandSubscriptions brandSubscriptions) {
		brandSubscriptionsRepo.delete(brandSubscriptions);
	}

	@Override
	public Optional<BrandSubscriptions> getBrandSubscriptionsByBrandIdAndActiveIsTrue(UUID brandId) {
		return brandSubscriptionsRepo.findByKeyBrandIdAndKeyActiveIsTrue(brandId);
	}

	@Override
	public void saveBrandCredits(BrandCredits brandCredits) {
		brandCreditsRepo.save(brandCredits);
	}

	@Override
	public Optional<SubscriptionMaster> getSubscriptionMaster(SubscriptionMasterKey key) {
		return subscriptionMasterRepo.findById(key);
	}

	@Override
	public Optional<BrandCampaignDetailsMysql> getBrandCampaignDetailsMysqlByCampaignIdAndBrandId(String campaignId, String brandId) {
		return brandCampaignDetailsMysqlRepo.findByCampaignIdAndBrandId(campaignId, brandId);
	}

	@Override
	public Optional<PromoteInfluencersData> getPromoteInfluencersDataByCampaignIdAndInfluencerDeepSocialId(UUID campaignId,
			String influencerDeepSocialId) {
		return promoteInfluencersDataRepo.findByKeyCampaignIdAndKeyInfluencerDeepSocialId(campaignId, influencerDeepSocialId);
	}

	@Override
	public void savePromoteInfluencersData(PromoteInfluencersData promoteInfluencersData) {
		promoteInfluencersDataRepo.save(promoteInfluencersData);
	}

	@Override
	public Optional<MailTemplateMaster> getMailTemplateMasterByTemplateName(String name) {
		return mailTemplateMasterRepo.findByTemplateName(name);
	}
}