package io.smartfluence.accounts.dao;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.stereotype.Repository;

import io.smartfluence.cassandra.entities.AudienceReportHistory;
import io.smartfluence.cassandra.repository.AudienceReportHistoryRepo;
import io.smartfluence.mysql.entities.APIServiceEmailValidation;
import io.smartfluence.mysql.entities.APIServiceIPValidation;
import io.smartfluence.mysql.entities.APIServiceMaster;
import io.smartfluence.mysql.entities.IndustryMaster;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.primary.APIServiceEmailValidationKey;
import io.smartfluence.mysql.entities.primary.APIServiceIPValidationKey;
import io.smartfluence.mysql.repository.APIServiceEmailValidationRepo;
import io.smartfluence.mysql.repository.APIServiceIPValidationRepo;
import io.smartfluence.mysql.repository.APIServiceMasterRepo;
import io.smartfluence.mysql.repository.IndustryMasterRepo;
import io.smartfluence.mysql.repository.MailTemplateMasterRepo;

@Repository
public class ToolsAPIDaoImpl implements ToolsAPIDao {

	@Autowired
	private APIServiceMasterRepo apiServiceMasterRepo;

	@Autowired
	private APIServiceEmailValidationRepo apiServiceEmailValidationRepo;

	@Autowired
	private APIServiceIPValidationRepo apiServiceIPValidationRepo;

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@Autowired
	private AudienceReportHistoryRepo audienceReportHistoryRepo;

	@Autowired
	private IndustryMasterRepo industryMasterRepo;

	@Autowired
	private MailTemplateMasterRepo mailTemplateMasterRepo;

	@Override
	public APIServiceMaster getApiServiceMasterByServiceName(String service) {
		return apiServiceMasterRepo.findByService(service).get();
	}

	@Override
	public Optional<APIServiceEmailValidation> getAPIServiceEmailValidation(String email, String service) {
		return apiServiceEmailValidationRepo.findByKeyEmailIdAndKeyService(email, service);
	}

	@Override
	public void saveAPIServiceEmailValidation(String email, String service) {
		Optional<APIServiceEmailValidation> optionalAPIServiceEmailValidation = getAPIServiceEmailValidation(email, service);

		if (optionalAPIServiceEmailValidation.isPresent())
			updateAPIServiceEmailValidation(optionalAPIServiceEmailValidation.get());
		else {
			apiServiceEmailValidationRepo.save(APIServiceEmailValidation.builder().key(APIServiceEmailValidationKey.builder()
					.emailId(email).service(service).build()).dailyCount(1).totalCount(1).createdAt(new Date()).build());
		}
	}

	private void updateAPIServiceEmailValidation(APIServiceEmailValidation apiServiceEmailValidation) {
		apiServiceEmailValidation.setDailyCount(apiServiceEmailValidation.getDailyCount() + 1);
		apiServiceEmailValidation.setTotalCount(apiServiceEmailValidation.getTotalCount() + 1);
		apiServiceEmailValidation.setModifiedAt(new Date());

		apiServiceEmailValidationRepo.save(apiServiceEmailValidation);
	}

	@Override
	public Optional<APIServiceIPValidation> getAPIServiceIPValidation(String ip, String service) {
		return apiServiceIPValidationRepo.findByKeyIpAddressAndKeyService(ip, service);
	}

	@Override
	public void saveAPIServiceIPValidation(String ip, String service) {
		Optional<APIServiceIPValidation> optionalAPIServiceIPValidation = getAPIServiceIPValidation(ip, service);

		if (optionalAPIServiceIPValidation.isPresent())
			updateAPIServiceIPValidation(optionalAPIServiceIPValidation.get());
		else {
			apiServiceIPValidationRepo.save(APIServiceIPValidation.builder().key(APIServiceIPValidationKey.builder()
					.ipAddress(ip).service(service).build()).dailyCount(1).totalCount(1).createdAt(new Date()).build());
		}
	}

	private void updateAPIServiceIPValidation(APIServiceIPValidation apiServiceIPValidation) {
		apiServiceIPValidation.setDailyCount(apiServiceIPValidation.getDailyCount() + 1);
		apiServiceIPValidation.setTotalCount(apiServiceIPValidation.getTotalCount() + 1);
		apiServiceIPValidation.setModifiedAt(new Date());

		apiServiceIPValidationRepo.save(apiServiceIPValidation);
	}

	@Override
	public void saveAudienceHistory(AudienceReportHistory audienceReportHistory) {
		audienceReportHistoryRepo.save(audienceReportHistory);
	}

	@Override
	public Boolean isAudienceHistoryExist(String emailId) {
		return Objects.nonNull(audienceReportHistoryRepo.findByKeyBusinessEmailAddress(emailId));
	}

	@Override
	public Optional<IndustryMaster> getIndustryMaster(String industry) {
		return industryMasterRepo.findByIndustry(industry);
	}

	@Override
	public Optional<MailTemplateMaster> getMailTemplateMasterByTemplateName(String name) {
		return mailTemplateMasterRepo.findByTemplateName(name);
	}
}