package io.smartfluence.accounts.dao;

import java.util.Optional;

import io.smartfluence.cassandra.entities.AudienceReportHistory;
import io.smartfluence.mysql.entities.APIServiceEmailValidation;
import io.smartfluence.mysql.entities.APIServiceIPValidation;
import io.smartfluence.mysql.entities.APIServiceMaster;
import io.smartfluence.mysql.entities.IndustryMaster;
import io.smartfluence.mysql.entities.MailTemplateMaster;

public interface ToolsAPIDao {

	APIServiceMaster getApiServiceMasterByServiceName(String service);

	Optional<APIServiceEmailValidation> getAPIServiceEmailValidation(String email, String service);

	Optional<APIServiceIPValidation> getAPIServiceIPValidation(String ip, String service);

	void saveAudienceHistory(AudienceReportHistory audienceReportHistory);

	Boolean isAudienceHistoryExist(String emailId);

	Optional<IndustryMaster> getIndustryMaster(String industry);

	Optional<MailTemplateMaster> getMailTemplateMasterByTemplateName(String name);

	void saveAPIServiceEmailValidation(String email, String service);

	void saveAPIServiceIPValidation(String ip, String service);
}