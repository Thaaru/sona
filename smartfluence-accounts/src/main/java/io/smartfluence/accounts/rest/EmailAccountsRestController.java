package io.smartfluence.accounts.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.smartfluence.accounts.service.EmailAccountsService;
import io.smartfluence.modal.ResetPasswordModal;
import io.smartfluence.modal.validation.ForgotPasswordModal;
import io.smartfluence.modal.validation.RegistrationModal;

@RestController
public class EmailAccountsRestController {

	@Autowired
	private EmailAccountsService emailAccountsService;

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@PostMapping("register")
	public synchronized ResponseEntity<Object> register(@Validated @ModelAttribute RegistrationModal registrationModal,
			BindingResult bindingResult, RedirectAttributes attributes, HttpServletRequest request) {
		if (!bindingResult.hasFieldErrors()) {
			registrationModal.setTimeZone(RequestContextUtils.getTimeZone(request));
			emailAccountsService.registerUserAccounts(registrationModal);

			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add(HttpHeaders.LOCATION, ServletUriComponentsBuilder.fromCurrentContextPath()
					.host(eurekaInstanceConfigBean.getHostname()).port(-1).toUriString());
			return new ResponseEntity<>(headers, HttpStatus.OK);
		}
		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("verify-email")
	public ResponseEntity<Object> verifyEmail(
			@RequestParam(required = false, name = "verificationCode") String verificationCode) {
		if (verificationCode != null) {
			emailAccountsService.verifyEmail(verificationCode);
			HttpHeaders header = new HttpHeaders();
			header.add("location", ServletUriComponentsBuilder.fromCurrentContextPath().path("login").toUriString());
			return new ResponseEntity<>(header, HttpStatus.PERMANENT_REDIRECT);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("forgot-password")
	public ResponseEntity<Object> forgotPassword(@Validated @ModelAttribute ForgotPasswordModal forgotPasswordModal,
			BindingResult bindingResult, RedirectAttributes attributes) {

		if (!bindingResult.hasFieldErrors())
			return emailAccountsService.forgotPassword(forgotPasswordModal);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("reset-password")
	public ResponseEntity<Object> resetPassword(@Validated @ModelAttribute ResetPasswordModal resetPasswordModal,
			BindingResult bindingResult, RedirectAttributes attributes) {

		if (!bindingResult.hasFieldErrors())
			return emailAccountsService.resetPassword(resetPasswordModal);

		return new ResponseEntity<>(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("accept-offer")
	public void updateInfluencerInterest(@RequestParam(required = false, name = "inviteLink") String inviteLink) {
		emailAccountsService.promoteCampaignInfluencerInterest(inviteLink);
	}

	@PostMapping("update-brand-subscriptions")
	public ResponseEntity<Object> updateBrandSubscriptions(@RequestParam String userName, @RequestParam String password, @RequestParam String subscriptions) {
		return emailAccountsService.updateBrandSubscriptions(userName, password, subscriptions);
	}
}