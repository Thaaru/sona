package io.smartfluence.accounts.rest;

import java.util.LinkedList;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.smartfluence.modal.ExceptionResponse;

@RestControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ConstraintViolationException.class)
	protected ResponseEntity<Object> handleVaidationContraintsException(ConstraintViolationException ex) {
		List<String> details = new LinkedList<>();
		ex.getConstraintViolations().forEach(e -> details.add(e.getMessage()));
		return new ResponseEntity<>(new ExceptionResponse(HttpStatus.BAD_REQUEST.getReasonPhrase(), details),
				HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		List<String> details = new LinkedList<>();
		ex.getBindingResult().getFieldErrors().forEach(e -> details.add(e.getDefaultMessage()));
		return new ResponseEntity<>(new ExceptionResponse(status.getReasonPhrase(), details), headers, status);
	}
}