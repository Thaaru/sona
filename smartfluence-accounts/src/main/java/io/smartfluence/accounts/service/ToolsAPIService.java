package io.smartfluence.accounts.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.modal.validation.DownloadReportModal;
import io.smartfluence.modal.validation.GenerateReportModal;
import io.smartfluence.modal.validation.InfluencerDataModal;
import io.smartfluence.modal.validation.InfluencerLookalikeModal;
import io.smartfluence.modal.validation.InfluencerOverlapModal;

public interface ToolsAPIService {

	ResponseEntity<Object> verifyUserRecaptcha(String value);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	ResponseEntity<Object> getInfluencerData(InfluencerDataModal influencerDataModal, HttpServletRequest servletRequest);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	ResponseEntity<Object> getInfluencersOverlap(InfluencerOverlapModal influencerOverlapModal, HttpServletRequest servletRequest);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	ResponseEntity<Object> getInfluencerslookalike(InfluencerLookalikeModal influencerLookalikeModal, HttpServletRequest servletRequest);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	ResponseEntity<Object> getAudienceReportId(GenerateReportModal generateReportModal);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	ResponseEntity<Object> getAudienceReport(DownloadReportModal downloadReportModal, HttpServletRequest servletRequest);
}