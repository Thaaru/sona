package io.smartfluence.accounts.service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.google.common.collect.Sets;

import io.smartfluence.accounts.dao.EmailAccountsDao;
import io.smartfluence.accounts.util.CronUtil;
import io.smartfluence.cassandra.entities.InfluencerDetails;
import io.smartfluence.cassandra.entities.PromoteInfluencersData;
import io.smartfluence.cassandra.entities.SubscriptionMaster;
import io.smartfluence.cassandra.entities.brand.BrandCredits;
import io.smartfluence.cassandra.entities.brand.BrandDetails;
import io.smartfluence.cassandra.entities.brand.BrandSubscriptions;
import io.smartfluence.cassandra.entities.brand.primary.BrandSubscriptionsKey;
import io.smartfluence.cassandra.entities.primary.SubscriptionMasterKey;
import io.smartfluence.cassandra.entities.primary.UserDetailsKey;
import io.smartfluence.cassandra.repository.brand.BrandDetailsRepo;
import io.smartfluence.constants.CampaignStatus;
import io.smartfluence.constants.InviteStatus;
import io.smartfluence.constants.PromoteInfluencerStatus;
import io.smartfluence.constants.SubscriptionType;
import io.smartfluence.constants.UserStatus;
import io.smartfluence.exception.IllegalActionException;
import io.smartfluence.exception.InvalidUserException;
import io.smartfluence.exception.UserAlreadyExistsException;
import io.smartfluence.mail.MailService;
import io.smartfluence.modal.ResetPasswordModal;
import io.smartfluence.modal.validation.ForgotPasswordModal;
import io.smartfluence.modal.validation.RegistrationModal;
import io.smartfluence.mysql.entities.BrandCampaignDetailsMysql;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.UserAccountsRepo;
import io.smartfluence.util.EmailUtil;
import io.smartfluence.util.VerificationUtil;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Component
public class EmailAccountsServiceImpl implements EmailAccountsService {

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserAccountsRepo userAccountsRepo;

	@Autowired
	private EmailAccountsDao emailAccountsDao;

	@Autowired
	private MailService mailService;

	@Autowired
	private EmailUtil emailUtil;

	@Autowired
	private CronUtil cronUtil;

	@Autowired
	private BrandDetailsRepo brandDetailsRepo;

	private static final String INFLUENCERACCEPTEDTEMPLATE = "InfluencerAccepted";

	@Override
	public void registerUserAccounts(RegistrationModal registrationModal) {
		String email = registrationModal.getEmail();

		checkFreeAccount(registrationModal);

		if (emailUtil.emailValidation(email.toLowerCase())) {
			if (userAccountsRepo.findByEmail(email) == null)
				registerUser(registrationModal);
			else
				throw new UserAlreadyExistsException("Your provided Email " + email + " has already been used.");
		} else
			throw new UserAlreadyExistsException("Enter valid business email address");
	}

	private void checkFreeAccount(RegistrationModal registrationModal) {
		if (registrationModal.getIsFreeAccount()) {
			try {
				Optional<SubscriptionMaster> subscriptionMaster = emailAccountsDao
						.getSubscriptionMaster(new SubscriptionMasterKey(SubscriptionType.FREE,
								UUID.fromString(registrationModal.getFreeSubscriptionCode())));

				if (!subscriptionMaster.isPresent())
					throw new UserAlreadyExistsException("Invalid link for free Sign-Up");
			} catch (Exception e) {
				throw new UserAlreadyExistsException("Invalid link for free Sign-Up");
			}
		}
	}

	private void registerUser(RegistrationModal registrationModal) {
		log.info("Registering new account {}", registrationModal.getBrandName());
		UserAccounts userAccounts = saveUserAccount(registrationModal);
		if (registrationModal.isBrand())
			saveBrand(userAccounts);
		else
			saveInfluencer(userAccounts);
		mailService.sendRegistrationMail(userAccounts);
	}

	private UserAccounts saveUserAccount(RegistrationModal registrationModal) {
		if (registrationModal.isBrand()) {
			if (!(registrationModal.getWebsite().contains("http")
					|| registrationModal.getWebsite().contains("https"))) {
				String website = "https://" + registrationModal.getWebsite();
				registrationModal.setWebsite(website);
			}
		}
		UserAccounts userAccounts = new UserAccounts();
		userAccounts.setBrandName(registrationModal.getBrandName());
		userAccounts.setFirstName(registrationModal.getFirstName());
		userAccounts.setLastName(registrationModal.getLastName());
		userAccounts.setIndustry(registrationModal.getIndustry());
		userAccounts.setEmail(registrationModal.getEmail());
		userAccounts.setPassword(passwordEncoder.encode(registrationModal.getPassword()));
		userAccounts.setUserType(registrationModal.getUserType());
		userAccounts.setInviteId(registrationModal.getFreeSubscriptionCode());
		userAccounts.setInviteStatus(registrationModal.getIsFreeAccount() ? InviteStatus.FREE : InviteStatus.UNINVITED);
		userAccounts.setUserStatus(UserStatus.REGISTERED);
		userAccounts.setVerificationCode(VerificationUtil.generateVCForRegistartion());
		userAccounts.setWebsite(registrationModal.getWebsite());
		userAccounts.setTimeZone(registrationModal.getTimeZone().getID());
		userAccounts.setCreatedAt(new Date());
		userAccounts = emailAccountsDao.saveUserAccounts(userAccounts);

		return userAccounts;
	}

	private void saveInfluencer(UserAccounts userAccounts) {
		InfluencerDetails influencerDetails = new InfluencerDetails();
		UserDetailsKey key = new UserDetailsKey(UserStatus.REGISTERED, UUID.fromString(userAccounts.getUserId()));
		influencerDetails.setKey(key);
		influencerDetails.setLastName(userAccounts.getLastName());
		influencerDetails.setEmail(userAccounts.getEmail());
		influencerDetails.setFirstName(userAccounts.getFirstName());
		influencerDetails.setIndustry(userAccounts.getIndustry());
		influencerDetails.setTimeZone(userAccounts.getTimeZone());

		emailAccountsDao.saveInfluencerDetails(influencerDetails);
	}

	private void saveBrand(UserAccounts userAccounts) {
		BrandDetails brandDetails = new BrandDetails();
		UserDetailsKey key = new UserDetailsKey(UserStatus.REGISTERED, UUID.fromString(userAccounts.getUserId()));
		brandDetails.setKey(key);
		brandDetails.setBrandName(userAccounts.getBrandName());
		brandDetails.setEmail(userAccounts.getEmail());
		brandDetails.setIndustry(userAccounts.getIndustry());
		brandDetails.setWebsite(userAccounts.getWebsite());
		brandDetails.setTimeZone(userAccounts.getTimeZone());
		brandDetails.setCreatedAt(new Date());
		brandDetails.setInviteStatus(userAccounts.getInviteStatus());

		emailAccountsDao.saveBrandDetails(brandDetails);
	}

	@Override
	public void verifyEmail(String verificationCode) {
		UserAccounts userAccounts = emailAccountsDao.getUserAccountsByVC(verificationCode);
		if (userAccounts != null && VerificationUtil.isRegistrationVC(verificationCode)) {
			updateUserAccountsToVerified(userAccounts);

			if (userAccounts.isBrand())
				updateBrandDetailsToVerified(userAccounts);
			else
				updateInfluencerDetailsToVerified(userAccounts);

			//mailService.sendAccountVerifiedMail(userAccounts);
		}
		else
			throw new UserAlreadyExistsException("Invalid email verification URL");
	}

	private void updateUserAccountsToVerified(UserAccounts userAccounts) {
		userAccounts.setUserStatus(userAccounts.isBrand() ? UserStatus.PENDING : UserStatus.APPROVED);
		userAccounts.setVerificationCode(null);
		userAccounts.setModifiedAt(new Date());

		emailAccountsDao.saveUserAccounts(userAccounts);
	}

	private void updateBrandDetailsToVerified(UserAccounts userAccounts) {
		Optional<BrandDetails> brandDetailsOptional = emailAccountsDao.getBrandDetailsById(new UserDetailsKey(UserStatus.REGISTERED,
				UUID.fromString(userAccounts.getUserId())));
		if (brandDetailsOptional.isPresent()) {
			BrandDetails brandDetails = brandDetailsOptional.get();
			BrandDetails brandDetailsUpdate = new BrandDetails(brandDetails);
			brandDetailsUpdate.getKey().setUserStatus(UserStatus.PENDING);
			brandDetailsUpdate.setModifiedAt(new Date());
			emailAccountsDao.deleteBrandDetails(brandDetails);
			emailAccountsDao.saveBrandDetails(brandDetailsUpdate);

			if (userAccounts.getInviteStatus().equals(InviteStatus.FREE))
				approveBrand(brandDetails.getKey().getUserId(), SubscriptionType.FREE, UUID.fromString(userAccounts.getInviteId()));

			mailService.sendBrandRegistrationMail(userAccounts);
		}
	}

	private void updateInfluencerDetailsToVerified(UserAccounts userAccounts) {
		Optional<InfluencerDetails> influencerDetailsOptional = emailAccountsDao.getInfluencerDetailsById
				(new UserDetailsKey(UserStatus.REGISTERED, UUID.fromString(userAccounts.getUserId())));
		if (influencerDetailsOptional.isPresent()) {
			InfluencerDetails influencerDetails = influencerDetailsOptional.get();
			InfluencerDetails influencerDetailsUpdate = new InfluencerDetails(influencerDetails);
			influencerDetailsUpdate.getKey().setUserStatus(UserStatus.APPROVED);

			emailAccountsDao.deleteInfluencerDetails(influencerDetails);
			emailAccountsDao.saveInfluencerDetails(influencerDetailsUpdate);
		}
	}

	private void approveBrand(UUID brandId, SubscriptionType subscriptionType, UUID subscriptionId) {

		try {
			Optional<UserAccounts> optionalUserAccounts = emailAccountsDao.getUserAccountsById(brandId.toString());

			if (optionalUserAccounts.isPresent()) {
				try {
					UserAccounts userAccounts = optionalUserAccounts.get();
					validateBrandStatus(userAccounts);
					BrandSubscriptions brandSubscriptions = createSubscription(brandId, subscriptionId,
							subscriptionType, userAccounts);
					updateApproveStatus(userAccounts, brandSubscriptions);
				} catch (Exception e) {
					throw new RuntimeException("The account that you are trying to approve is not found", e);
				}
			} else
				throw new IllegalActionException("The account that you are trying to approve is not found");
		} catch (Exception e) {
			log.error("Unable to approve brand on verification", e);
		}
	}

	private void validateBrandStatus(UserAccounts userAccounts) {
		if (!userAccounts.isBrand())
			throw new IllegalActionException("The account that you are trying to approve is not a brand");

		if (userAccounts.getUserStatus().equals(UserStatus.REGISTERED))
			throw new IllegalActionException("The account that you are trying to approve is not verified");
	}

	private BrandSubscriptions createSubscription(UUID brandId, UUID subscriptionId, SubscriptionType subscriptionType,
			UserAccounts userAccounts) {
		SubscriptionMaster master = emailAccountsDao.getSubscriptionMaster(new SubscriptionMasterKey(subscriptionType, subscriptionId)).get();
		Optional<BrandSubscriptions> optionalOldBrandSubscriptions = emailAccountsDao.getBrandSubscriptionsByBrandIdAndActiveIsTrue(brandId);

		BrandSubscriptions brandSubscriptions = BrandSubscriptions.builder().brandName(userAccounts.getBrandName())
				.credits(master.getCredits()).renewable(master.isRenewable()).resetCredit(master.isResetCredit())
				.subscriptionDate(new Date()).subscriptionName(master.getSubscriptionName())
				.subscriptionType(subscriptionType).validity(master.getValidity())
				.validityType(master.getValidityType()).key(BrandSubscriptionsKey.builder().active(true)
						.brandId(brandId).subscriptionId(subscriptionId).transactionId(UUID.randomUUID()).build())
				.createdAt(new Date()).build();

		if (optionalOldBrandSubscriptions.isPresent()) {
			BrandSubscriptions oldBrandSubscriptions = optionalOldBrandSubscriptions.get();
			BrandSubscriptions oldUpdatedBrandSubscriptions = new BrandSubscriptions(oldBrandSubscriptions);
			oldUpdatedBrandSubscriptions.getKey().setActive(false);
			emailAccountsDao.deleteBrandSubscriptions(oldBrandSubscriptions);
			emailAccountsDao.saveBrandSubscriptions(oldUpdatedBrandSubscriptions);
		}
		emailAccountsDao.saveBrandSubscriptions(brandSubscriptions);

		BrandCredits brandCredits = BrandCredits.builder().brandId(brandId).credits(master.getCredits())
				.createdAt(new Date()).validUpto(Date.from(getLastDateOfSubscription(brandSubscriptions).toInstant()))
				.build();
		emailAccountsDao.saveBrandCredits(brandCredits);

		return brandSubscriptions;
	}

	private void updateApproveStatus(UserAccounts userAccounts, BrandSubscriptions brandSubscriptions)
			throws CloneNotSupportedException {

		Optional<BrandDetails> optionalBrandDetails = emailAccountsDao.getBrandDetailsByUserStatusInAndUserId(
				Sets.newHashSet(UserStatus.APPROVED, UserStatus.DECLINED, UserStatus.INACTIVE, UserStatus.PENDING),
				UUID.fromString(userAccounts.getUserId()));

		if (optionalBrandDetails.isPresent()) {
			BrandDetails brandDetails = optionalBrandDetails.get();
			BrandDetails updateBrandDetails = (BrandDetails) brandDetails.copy();
			updateBrandDetails.setSubscriptionId(brandSubscriptions.getKey().getSubscriptionId());
			updateBrandDetails.setSubscriptionType(brandSubscriptions.getSubscriptionType());
			updateBrandDetails.setSubscriptionName(brandSubscriptions.getSubscriptionName());
			updateBrandDetails.setValidUpto(Date.from(getLastDateOfSubscription(brandSubscriptions).toInstant()));
			updateBrandDetails.getKey().setUserStatus(UserStatus.APPROVED);
			updateBrandDetails.setModifiedAt(new Date());

			if (!brandDetails.getKey().getUserStatus().equals(updateBrandDetails.getKey().getUserStatus())) {
				emailAccountsDao.deleteBrandDetails(brandDetails);
				emailAccountsDao.saveBrandDetails(updateBrandDetails);
			}
			userAccounts.setUserStatus(UserStatus.APPROVED);
			emailAccountsDao.saveUserAccounts(userAccounts);

			sendApprovedMailToBrand(userAccounts, brandSubscriptions, optionalBrandDetails.get().getKey().getUserStatus().equals(UserStatus.PENDING));
		}
	}

	private void sendApprovedMailToBrand(UserAccounts userAccounts, BrandSubscriptions brandSubscriptions, Boolean newUser) {

		if (brandSubscriptions.getSubscriptionName().equals("Trial"))
			mailService.sendApprovedMailForTrail(userAccounts, newUser);
		else if (brandSubscriptions.getSubscriptionName().equals("Free_Account"))
			mailService.sendApprovedMailForSmartfluenceFree(userAccounts, newUser);
		else
			mailService.sendApprovedMailForSmartfluence(userAccounts, newUser);
	}

	private ZonedDateTime getLastDateOfSubscription(BrandSubscriptions subscriptions) {
		Date date = subscriptions.getModifiedAt() == null ? subscriptions.getCreatedAt()
				: subscriptions.getModifiedAt();
		ZonedDateTime zoneDateTime = date.toInstant().atZone(ZoneId.systemDefault()).withHour(0).withMinute(0)
				.withSecond(0).withNano(0);
		switch (subscriptions.getValidityType()) {
		case DAY:
			zoneDateTime = zoneDateTime.plusDays(subscriptions.getValidity());
			break;
		case MONTH:
			zoneDateTime = zoneDateTime.plusMonths(subscriptions.getValidity());
			break;
		case YEAR:
			zoneDateTime = zoneDateTime.plusYears(subscriptions.getValidity());
			break;
		case HOUR:
			zoneDateTime = zoneDateTime.plusHours(subscriptions.getValidity());
			break;
		case MINUTE:
			zoneDateTime = zoneDateTime.plusMinutes(subscriptions.getValidity());
			break;
		default:
			break;
		}
		return zoneDateTime;
	}

	@Override
	public ResponseEntity<Object> forgotPassword(ForgotPasswordModal forgotPasswordModal) {
		UserAccounts userAccounts = emailAccountsDao.getUserAccountsByEmail(forgotPasswordModal.getEmail());

		if (userAccounts != null && !UserStatus.REGISTERED.equals(userAccounts.getUserStatus())) {
			userAccounts.setVerificationCode(VerificationUtil.generateVCForForgotPassword());
			userAccounts = emailAccountsDao.saveUserAccounts(userAccounts);
			mailService.sendForgotPasswordMail(userAccounts);

			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add(HttpHeaders.LOCATION, ServletUriComponentsBuilder.fromCurrentContextPath()
					.host(eurekaInstanceConfigBean.getHostname()).port(-1).toUriString());
			return new ResponseEntity<>(headers, HttpStatus.OK);
		} else
			throw new InvalidUserException("Invalid User");
	}

	@Override
	public ResponseEntity<Object> resetPassword(ResetPasswordModal resetPasswordModal) {
		UserAccounts userAccounts = emailAccountsDao.getUserAccountsByVC(resetPasswordModal.getVerificationCode());
		if (userAccounts != null) {
			userAccounts.setPassword(passwordEncoder.encode(resetPasswordModal.getPassword()));
			userAccounts.setVerificationCode(null);
			userAccounts.setModifiedAt(new Date());
			emailAccountsDao.saveUserAccounts(userAccounts);
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add(HttpHeaders.LOCATION, ServletUriComponentsBuilder.fromCurrentContextPath()
					.host(eurekaInstanceConfigBean.getHostname()).port(-1).toUriString());
			return new ResponseEntity<>(headers, HttpStatus.OK);
		} else
			throw new InvalidUserException("Invalid User");
	}

	@Override
	public void promoteCampaignInfluencerInterest(String influencerInviteLink) {
		if (!StringUtils.isEmpty(influencerInviteLink)) {
			String inviteLinkValues [] = influencerInviteLink.split("_");

			if (inviteLinkValues.length == 3) {
				Optional<BrandCampaignDetailsMysql> brandCampaignDetailsMysql = emailAccountsDao.
						getBrandCampaignDetailsMysqlByCampaignIdAndBrandId(inviteLinkValues[1], inviteLinkValues[0]);

				if (validatePromoteCampaignStatus(brandCampaignDetailsMysql)) {
					Optional<BrandDetails> brandDetails = emailAccountsDao.
							getBrandDetailsById(new UserDetailsKey(UserStatus.APPROVED, UUID.fromString(inviteLinkValues[0])));

					if (brandDetails.isPresent())
						changePromoteCampaignInfluencerStatus(brandCampaignDetailsMysql.get(), inviteLinkValues[2], brandDetails.get());
					else
						throw new UserAlreadyExistsException("Campaign has been closed");
				}
			} else
				throw new UserAlreadyExistsException("Invalid Invite link");
		} else
			throw new UserAlreadyExistsException("Invalid Invite link");
	}

	private boolean validatePromoteCampaignStatus(Optional<BrandCampaignDetailsMysql> brandCampaignDetailsMysql) {
		if (brandCampaignDetailsMysql.isPresent()) {
			if (brandCampaignDetailsMysql.get().getCampaignStatus().equals(CampaignStatus.CLOSED)
					|| StringUtils.isEmpty(brandCampaignDetailsMysql.get().getActiveUntil())
					|| brandCampaignDetailsMysql.get().getActiveUntil().toInstant().isBefore(Instant.now()))
				throw new UserAlreadyExistsException("Campaign has been closed");
		} else
			throw new UserAlreadyExistsException("Campaign not available");

		return true;
	}

	private void changePromoteCampaignInfluencerStatus(BrandCampaignDetailsMysql brandCampaignDetailsMysql, String influencerDeepSocialId,
			BrandDetails brandDetails) {
		Optional<PromoteInfluencersData> optioanlPromoteInfluencersData = emailAccountsDao.
				getPromoteInfluencersDataByCampaignIdAndInfluencerDeepSocialId(UUID.fromString(brandCampaignDetailsMysql.getCampaignId()),
						influencerDeepSocialId);

		if (optioanlPromoteInfluencersData.isPresent()) {
			if (!optioanlPromoteInfluencersData.get().getStatus().equals(PromoteInfluencerStatus.SENT))
				throw new UserAlreadyExistsException("Invalid Invite link");

			PromoteInfluencersData promoteInfluencersData = optioanlPromoteInfluencersData.get();
			promoteInfluencersData.setStatus(PromoteInfluencerStatus.PENDING);
			promoteInfluencersData.setModifiedAt(new Date());
			emailAccountsDao.savePromoteInfluencersData(promoteInfluencersData);

			sendNotificationMailToBrand(brandDetails, brandCampaignDetailsMysql, promoteInfluencersData);
		} else
			throw new UserAlreadyExistsException("Invalid Invite link");
	}

	private void sendNotificationMailToBrand(BrandDetails brandDetails, BrandCampaignDetailsMysql brandCampaignDetailsMysql,
			PromoteInfluencersData promoteInfluencersData) {
		Optional<MailTemplateMaster> mailTemplateMaster = emailAccountsDao.getMailTemplateMasterByTemplateName(INFLUENCERACCEPTEDTEMPLATE);

		MailDetails mailDetails = MailDetails.builder()
				.brandEmail(brandDetails.getEmail()).brandId(brandDetails.getKey().getUserId().toString())
				.brandName(brandDetails.getBrandName()).campaignName(brandCampaignDetailsMysql.getCampaignName())
				.influencerHandle(promoteInfluencersData.getInfluencerName())
				.mailTemplateId(UUID.fromString(mailTemplateMaster.get().getId()).toString())
				.mailSubject(mailTemplateMaster.get().getMailSubject())
				.mailDescription(mailTemplateMaster.get().getMailBody())
				.postType(brandCampaignDetailsMysql.getPostType())
				.status(32).mappingId(promoteInfluencersData.getKey().getCampaignId().toString())
				.payment(brandCampaignDetailsMysql.getPayment()).build();
		
		mailService.sendInfluencerPendingMailToBrand(mailDetails);
	}

	@Override
	public ResponseEntity<Object> updateBrandSubscriptions(String userName, String password, String subscriptions) {
		if (validateUser(userName, password)) {
			brandDetailsRepo.findAllByKeyUserStatusIn(Sets.newHashSet(UserStatus.APPROVED, UserStatus.INACTIVE)).stream()
				.filter(f -> f.getSubscriptionType().name().equalsIgnoreCase(subscriptions)).forEach(data -> {
					try {
						Optional<BrandSubscriptions> brandSubscriptions = emailAccountsDao
								.getBrandSubscriptionsByBrandIdAndActiveIsTrue(data.getKey().getUserId());
						cronUtil.updateSubscriptions(data, brandSubscriptions.get());
					} catch (CloneNotSupportedException e) {
						log.error(e);
					}
				});
			return ResponseEntity.status(HttpStatus.OK).body("Brands subscriptions has been updated...!!");
		}

		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("You are not authorized..!");
	}

	private boolean validateUser(String userName, String password) {
		try {
			UserAccounts userAccounts = userAccountsRepo.findByEmail(userName);

			if (passwordEncoder.matches(password, userAccounts.getPassword())) {
				if (userAccounts.getEmail().contains("kirupa") || userAccounts.getEmail().contains("uday"))
					return true;
			}

			return false;
		} catch (Exception e) {
			log.error("User not available");
			return false;
		}
	}
}