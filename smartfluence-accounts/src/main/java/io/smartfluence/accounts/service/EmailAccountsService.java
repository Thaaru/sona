package io.smartfluence.accounts.service;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.smartfluence.modal.ResetPasswordModal;
import io.smartfluence.modal.validation.ForgotPasswordModal;
import io.smartfluence.modal.validation.RegistrationModal;

public interface EmailAccountsService {

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void registerUserAccounts(RegistrationModal registrationModal);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void verifyEmail(String verificationCode);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	ResponseEntity<Object> forgotPassword(ForgotPasswordModal forgotPasswordModal);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	ResponseEntity<Object> resetPassword(ResetPasswordModal resetPasswordModal);

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	void promoteCampaignInfluencerInterest(String influencerInviteLink);

	ResponseEntity<Object> updateBrandSubscriptions(String userName, String password, String subscriptions);
}