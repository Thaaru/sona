package io.smartfluence.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.smartfluence.cassandra.entities.MailHistory;
import io.smartfluence.cassandra.entities.primary.MailHistoryKey;
import io.smartfluence.cassandra.repository.MailHistoryRepo;
import io.smartfluence.constants.InviteStatus;
import io.smartfluence.constants.UserType;
import io.smartfluence.modal.validation.DownloadReportModal;
import io.smartfluence.mysql.entities.MailDetails;
import io.smartfluence.mysql.entities.MailTemplateMaster;
import io.smartfluence.mysql.entities.UserAccounts;
import io.smartfluence.mysql.repository.MailTemplateMasterRepo;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
@Configuration
public class MailService {

	@Autowired
	private EurekaInstanceConfigBean instanceConfigBean;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	EurekaInstanceConfigBean eurekaInstanceConfigBean;

	@Autowired
	private MailHistoryRepo mailHistoryRepo;

	@Autowired
	private MailProperties mailProperties;

	@Autowired
	private MailTemplateMasterRepo mailTemplateMasterRepo;

	private static final String APP_SMARTFLUENCE_IO = "app.smartfluence.io";

	private static final String ADMIN_MAIL = "admin@smartfluence.io";

	private static final String ADMIN_MAIL_DAVID = "david@smartfluence.io";

	private static final String ADMIN_MAIL_SID = "siddharth@smartfluence.io";

	private static final String ADMIN_MAIL_TEST = "uday.dinesh@scriplogix.com";

	private static final String INFLUENCER_FROM_MAIL_ADDRESS_TEST = "kirupa.thiyagarajan@smartfluence.io";

	private static final String YEAR = "year";

	private static final String NAME = "Name";

	private static final String LINK = "link";

	private static final String BRAND_NAME = "brandName";

	private static final String BRANDNAME = "Brand-Name";

	private static final String BUSSINESS_EMAIL_ADDRESS = "Business-Email-Address";

	private static final String INFLUENCER_USERNAME = "Influencer-Username";

	@SuppressWarnings("unused")
	private static final String DESCRIBES = "Describes";

	@SuppressWarnings("unused")
	private static final String NO_OF_INFLUENCERS = "No-Of-Influencers";

	private static final String INFLUENCERNAME = "<Influencer_Username>";

	private static final String PROMOTEBRANDNAME = "<Brand_Name>";

	private static final String PAYMENT = "<Payment>";

	private static final String POSTTYPE = "<PostType>";

	private static final String HOMELINK = "<link>";

	private static final String MAIL_BODY = "mailBody";

	private static final String TEXT_HTML = "text/html";

	private static final String SMARTFLUENCE_DATA = "smartfluence-data";

	private static final String MAIL_TEMPLATE = "mail-template";

	private static final String ACCOUNTVERIFIEDMAIL = "accountVerifiedMail.html";

	private static final String REGISTRATIONMAIL = "registrationMail.html";

	private static final String FORGOTPASSWORDMAIL = "forgotPasswordMail.html";

	private static final String APPROVEDMAILFORTRIAL = "approvedMailForTrial.html";

	private static final String APPROVEDMAILFORSMARTFLUENCE = "approvedMailForSmartfluence.html";

	private static final String DOWNLOADREPORTMAIL = "downloadReportMail.html";

	private static final String BRANDNOTIFICATIONMAIL = "brandNotificationMail.html";

	private final Log logger = LogFactory.getLog(getClass());

	@Value("${report.mail.recipient}")
	private String reportMailRecipient;

	private Optional<MailTemplateMaster> getMailTemplateMaster(String templateName) {
		return mailTemplateMasterRepo.findByTemplateName(templateName);
	}

	public void sendAccountVerifiedMail(UserAccounts userAccounts) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> accountVerifiedMail(userAccounts, homeLink)).start();
	}

	public void sendRegistrationMail(UserAccounts userAccounts) {
		String verificationLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).path("verify-email")
				.queryParam("verificationCode", userAccounts.getVerificationCode()).build().encode().toUriString();
		new Thread(() -> registrationMail(userAccounts, verificationLink)).start();
	}

	public void sendForgotPasswordMail(UserAccounts userAccounts) {
		String link = ServletUriComponentsBuilder.fromCurrentContextPath().host(eurekaInstanceConfigBean.getHostname())
				.path("reset-password").queryParam("verificationCode", userAccounts.getVerificationCode()).build()
				.encode().toUriString();
		new Thread(() -> forgotPasswordMail(userAccounts, link)).start();
	}

	public void sendBrandRegistrationMail(UserAccounts userAccounts) {
		String link = ServletUriComponentsBuilder.fromCurrentContextPath().host(eurekaInstanceConfigBean.getHostname())
				.port(-1).pathSegment("admin", "brand-management").build().encode().toUriString();
		new Thread(() -> brandRegistrationMail(userAccounts, link)).start();
	}

	public void sendApprovedMailForTrail(UserAccounts userAccounts, Boolean newUser) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> approvedMailForTrail(userAccounts, homeLink, newUser)).start();
	}

	public void sendApprovedMailForSmartfluence(UserAccounts userAccounts, Boolean newUser) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> approvedMailForSmartfluence(userAccounts, homeLink, newUser)).start();
	}

	public void sendApprovedMailForSmartfluenceFree(UserAccounts userAccounts, Boolean newUser) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> approvedMailForSmartfluenceFree(userAccounts, homeLink, newUser)).start();
	}

	public void sendMailForReportDownload(DownloadReportModal downloadReportModal) {
		new Thread(() -> mailForReportDownload(downloadReportModal)).start();
	}

	public void sendInfluencerPendingMailToBrand(MailDetails mailDetails) {
		String homeLink = ServletUriComponentsBuilder.fromCurrentContextPath()
				.host(eurekaInstanceConfigBean.getHostname()).port(-1).build().encode().toUriString();
		new Thread(() -> submitInfluencerPendingMailToBrand(mailDetails, homeLink)).start();
	}

	private void accountVerifiedMail(UserAccounts userAccounts, String homeLink) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			mimeMessage.setSubject("Smartfluence: Verify your email");
			mimeMessage.setContent(getAccountVerifiedMail(userAccounts, homeLink), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void registrationMail(UserAccounts userAccounts, String verificationLink) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			mimeMessage.setSubject("Smartfluence: Verify your email");
			mimeMessage.setContent(getRegistrationMail(userAccounts, verificationLink), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void forgotPasswordMail(UserAccounts userAccounts, String link) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			mimeMessage.setSubject("Smartfluence: Forgot your password");
			mimeMessage.setContent(getForgotPasswordMail(userAccounts, link), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void brandRegistrationMail(UserAccounts userAccounts, String link) {
		try {
			MailTemplateMaster mailTemplateMaster = getMailTemplateMaster(userAccounts.getInviteStatus().equals(InviteStatus.FREE)
					? "BrandRegistrationMail-FreeAccount" : "BrandRegistrationMail").get();
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			setAdminMailRecepient(mimeMessage);
			mimeMessage.setFrom(mailProperties.getUsername());
			mimeMessage.setSubject(mailTemplateMaster.getMailSubject().replace("<brandName>", userAccounts.getBrandName())
					.replace("<brandEmail>", userAccounts.getEmail()));
			mimeMessage.setContent(getBrandRegistrationMail(userAccounts, link, mailTemplateMaster), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void approvedMailForTrail(UserAccounts userAccounts, String homeLink, Boolean newUser) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			if (newUser) {
				mimeMessage.setSubject("Welcome to Smartfluence!");
			} else {
				mimeMessage.setSubject("Smartfluence Platform");
			}
			mimeMessage.setContent(getApprovedMailForTrail(userAccounts, homeLink), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void approvedMailForSmartfluence(UserAccounts userAccounts, String homeLink, Boolean newUser) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			if (newUser)
				mimeMessage.setSubject("Welcome to Smartfluence!");
			else
				mimeMessage.setSubject("Smartfluence Platform");

			mimeMessage.setContent(getApprovedMailForSmartfluence(userAccounts, homeLink), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void approvedMailForSmartfluenceFree(UserAccounts userAccounts, String homeLink, Boolean newUser) {
		try {
			MailTemplateMaster mailTemplateMaster = getMailTemplateMaster("ApprovedMailSmartfluenceFree").get();
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(userAccounts.getEmail()));
			mimeMessage.setFrom(mailProperties.getUsername());
			if (newUser)
				mimeMessage.setSubject(mailTemplateMaster.getMailSubject().substring(0, mailTemplateMaster.getMailSubject().indexOf(",")));
			else
				mimeMessage.setSubject(mailTemplateMaster.getMailSubject().substring(mailTemplateMaster.getMailSubject().indexOf(",") + 1,
						mailTemplateMaster.getMailSubject().length()));

			mimeMessage.setContent(getApprovedMailForSmartfluenceFree(userAccounts, homeLink, mailTemplateMaster), TEXT_HTML);
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void mailForReportDownload(DownloadReportModal downloadReportModal) {
		try {
			File reportFile = setMailAttachment(downloadReportModal.getFile(), 
					downloadReportModal.getFileName());
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
			mimeMessageHelper.setTo(reportMailRecipient);
			mimeMessageHelper.setFrom(mailProperties.getUsername());
			mimeMessageHelper.setSubject("ATTENTION! - New Lead");
			mimeMessageHelper.setText(getDownloadReportMail(downloadReportModal), true);
			mimeMessageHelper.addAttachment(downloadReportModal.getFileName(), reportFile);
			mailSender.send(mimeMessage);

			Files.delete(reportFile.toPath());
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private void submitInfluencerPendingMailToBrand(MailDetails mailDetails, String homeLink) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			mimeMessage.setFrom(mailProperties.getUsername());
			mimeMessage.setSubject(mailDetails.getMailSubject().replaceAll(INFLUENCERNAME, mailDetails.getInfluencerHandle()));
			mimeMessage.setContent(getInfluencerPendingMailBody(mailDetails, homeLink), TEXT_HTML);
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(mailDetails.getBrandEmail()));

			if (!eurekaInstanceConfigBean.getHostname().equals(APP_SMARTFLUENCE_IO))
				mimeMessage.setRecipient(RecipientType.CC, new InternetAddress(INFLUENCER_FROM_MAIL_ADDRESS_TEST));

			mailHistoryRepo.save(MailHistory.builder()
					.mailHistoryKey(MailHistoryKey.builder().brandId(UUID.fromString(mailDetails.getBrandId()))
							.createdAt(new Date()).build())
					.mailBody(mailDetails.getMailDescription()).mailFrom(mailProperties.getUsername())
					.mailSubject(mailDetails.getMailSubject()).mailTemplateId(UUID.fromString(mailDetails.getMailTemplateId()))
					.mailTo(mailDetails.getBrandEmail()).status(mailDetails.getStatus()).mappingId(UUID.fromString(mailDetails.getMappingId())).build());
			mailSender.send(mimeMessage);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	private String getAccountVerifiedMail(UserAccounts userAccounts, String homeLink) throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + ACCOUNTVERIFIEDMAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue("embedlink", homeLink)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getRegistrationMail(UserAccounts userAccounts, String verificationLink) throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + REGISTRATIONMAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue(LINK, verificationLink)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getForgotPasswordMail(UserAccounts userAccounts, String link) throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + FORGOTPASSWORDMAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue("embedlink", link)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getBrandRegistrationMail(UserAccounts userAccounts, String link, MailTemplateMaster mailTemplateMaster) {
		return new MessageBuilder(mailTemplateMaster.getMailBody().replace("<brandName>", userAccounts.getBrandName())
				.replace("<firstname>", userAccounts.getFirstName()).replace("<lastname>", userAccounts.getLastName())
				.replace("<link>", link).replaceAll("<apostrophe>", "'").replace("<email>", userAccounts.getEmail())
				.replace("<industry>", userAccounts.getIndustry()).replace("<website>", userAccounts.getWebsite())
				.replace("<year>", getDateTimeValues(1).toString())).getMessage();
	}
	
	private String getApprovedMailForTrail(UserAccounts userAccounts, String homeLink) throws IOException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + APPROVEDMAILFORTRIAL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue(LINK, homeLink)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getApprovedMailForSmartfluence(UserAccounts userAccounts, String homeLink) throws IOException {
		String contentText = readFile(new ClassPathResource(SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE
				+ File.separatorChar + APPROVEDMAILFORSMARTFLUENCE).getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRAND_NAME, getName(userAccounts))
				.setValue(LINK, homeLink)
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getApprovedMailForSmartfluenceFree(UserAccounts userAccounts, String homeLink, MailTemplateMaster mailTemplateMaster) {
		return new MessageBuilder(mailTemplateMaster.getMailBody().replace("<brandName>", getName(userAccounts))
				.replace("<link>", homeLink).replaceAll("<apostrophe>", "'")
				.replace("<year>", getDateTimeValues(1).toString())).getMessage();
	}

	private String getDownloadReportMail(DownloadReportModal downloadReportModal) throws IOException, MessagingException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + DOWNLOADREPORTMAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(BRANDNAME, downloadReportModal.getBrandName())
				.setValue(BUSSINESS_EMAIL_ADDRESS, downloadReportModal.getBrandEmail())
				.setValue(INFLUENCER_USERNAME, downloadReportModal.getInfluencerHandle())
				.setValue(NAME, downloadReportModal.getPlatform())
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private String getInfluencerPendingMailBody(MailDetails mailDetails, String homeLink) throws IOException, MessagingException {
		String contentText = readFile(new ClassPathResource(
				SMARTFLUENCE_DATA + File.separatorChar + MAIL_TEMPLATE + File.separatorChar + BRANDNOTIFICATIONMAIL)
						.getInputStream());
		MessageBuilder messageBuilder = new MessageBuilder(contentText)
				.setValue(MAIL_BODY, mailDetails.getMailDescription().replace(PROMOTEBRANDNAME, mailDetails.getBrandName())
						.replace(INFLUENCERNAME, mailDetails.getInfluencerHandle()).replace(POSTTYPE, mailDetails.getPostType())
						.replace(PAYMENT, mailDetails.getPayment().toString()).replace(HOMELINK, homeLink).replaceAll("(\r\n|\n\r|\r|\n)", "<br>"))
				.setValue(YEAR, getDateTimeValues(1));
		return messageBuilder.getMessage();
	}

	private void setAdminMailRecepient(MimeMessage mimeMessage) throws MessagingException, AddressException {
		if (instanceConfigBean.getHostname().toLowerCase().trim().equals(APP_SMARTFLUENCE_IO)) {
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(ADMIN_MAIL));
			mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(ADMIN_MAIL_SID));
			mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(ADMIN_MAIL_DAVID));
		} else {
			mimeMessage.setRecipient(RecipientType.TO, new InternetAddress(ADMIN_MAIL_TEST));
			mimeMessage.addRecipient(RecipientType.TO, new InternetAddress(INFLUENCER_FROM_MAIL_ADDRESS_TEST));
		}
	}

	private String getName(UserAccounts userAccounts) {
		return userAccounts.getUserType().equals(UserType.BRAND) ? userAccounts.getBrandName()
				: userAccounts.getFirstName() + ' ' + userAccounts.getLastName();
	}

	private String readFile(InputStream file) {
		StringBuilder builder = new StringBuilder();
		try (BufferedReader r = new BufferedReader(new InputStreamReader(file))) {
			r.lines().forEach(builder::append);
		} catch (Exception e) {
			logger.error(e);
		}
		return builder.toString();
	}

	private Object getDateTimeValues(int flag) {

		if (flag == 0)
			return LocalDateTime.now();

		else if (flag == 1)
			return LocalDateTime.now().getYear();

		else if (flag == 2)
			return LocalDateTime.now().getDayOfMonth();

		else if (flag == 3)
			return LocalDateTime.now().getMonthValue();

		else if (flag == 4)
			return LocalDateTime.now().getDayOfWeek();

		else if (flag == 5)
			return LocalDateTime.now().getMonth();

		return "";
	}

	private File setMailAttachment(byte[] file, String fileName) {
		File reportFile = new File(fileName);
		try (FileOutputStream fileWriter = new FileOutputStream(reportFile)) {
			fileWriter.write(file);
		} catch (IOException e) {
			log.error("Error occurred while reading file, due to {}", e);
		}
		return reportFile;
	}

	class MessageBuilder {
		private StringBuilder builder;

		public MessageBuilder() {
			builder = new StringBuilder();
		}

		public MessageBuilder(String message) {
			builder = new StringBuilder();
			if (message != null)
				builder.append(message);
		}

		public MessageBuilder append(CharSequence charSequence) {
			builder.append(charSequence);
			return this;
		}

		public MessageBuilder setValue(String key, Object value) {
			int index = builder.indexOf(':' + key + ':');
			while (index != -1) {
				builder.replace(builder.indexOf(':' + key + ':'), builder.indexOf(':' + key + ':') + key.length() + 2,
						value == null ? "N/A" : String.valueOf(value));
				index = builder.indexOf(':' + key + ':');
			}
			return this;
		}

		public String getMessage() {
			return builder.toString();
		}
	}
}