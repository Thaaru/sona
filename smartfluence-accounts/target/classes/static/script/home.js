function rT() {
	$('#companyName').attr('required', 'required');
	$('#companyName').parent().show();
	$('#firstname').attr('required', 'required');
	$('#firstname').parent().show();
	$('#lastname').attr('required', 'required');
	$('#lastname').parent().show();
}

rT();

function formCallBack(obj) {
	if (obj.id == 'signInForm')
		signIn(obj)
	else if (obj.id == 'forgotForm')
		submitForgotPassword(obj);
	else
		signUp(obj);
	return false;
}

$(document)
		.ready(
				function() {
					toastr.options = {
						'closeButton' : true,
						'debug' : false,
						'newestOnTop' : false,
						'progressBar' : false,
						'positionClass' : 'toast-top-right',
						'preventDuplicates' : false,
						'showDuration' : '1000',
						'hideDuration' : '1000',
						'timeOut' : '10000',
						'extendedTimeOut' : '1000',
						'showEasing' : 'swing',
						'hideEasing' : 'linear',
						'showMethod' : 'fadeIn',
						'hideMethod' : 'fadeOut',
					}

					$("#isFreeAccount").val(isFree);
					$("#freeSubscriptionCode").val(subscriptionId);
					if (isFree == 'true') {
						$("#panelRegister").addClass("in active show");
						$("#panelLogin").removeClass("in active show");
					}
					$("#signIn").click(function() {
						$("#panelLogin").addClass("in active show");
						$("#panelRegister").removeClass("in active show");

					});
					if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
							.test(navigator.userAgent)) {
						toastr
								.warning('The Smartfluence Platform is recommended to be used on Desktops / Laptops only');
					}
					$("#signUp").click(function() {
						$("#panelRegister").addClass("in active show");
						$("#panelLogin").removeClass("in active show");
					});
				});

function signIn(obj) {

	$(obj).unbind('submit', formSubmit);
	$(obj).submit();
	return;

	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');

	xhr.done(function(d, s, r) {
		resetForms();
		window.location.href = r.getResponseHeader('location');
	});
	xhr
			.fail(function(x) {
				$('[type="password"]').val('');
				if (x.status == 401) {
					ERROR('The email or password is not a valid credentials');
				} else {
					ERROR('Our server is facing a technical issue. Please try again after a while')
				}
			});
}

function signUp(obj) {

	var businessEmail = $('#businessemail').val().toLowerCase();

	if (businessEmail.includes("@gmail.") || businessEmail.includes("@yahoo.")
			|| businessEmail.includes("@outlook.")
			|| businessEmail.includes("@icloud.")
			|| businessEmail.includes("@hotmail.")
			|| businessEmail.includes("@me.")
			|| businessEmail.includes("@mail.ru")) {
		ERROR('Enter valid business email address');
		return;
	}

	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');

	log(xhr);
	xhr.done(function(d, s, r) {
		window.location.href = window.location.origin + '/register-successful';
	});
	xhr
			.fail(function(x) {
				if (x.status == 409) {
					ERROR(x.responseJSON.message);
				} else {
					ERROR('Our server is facing a technical issue. Please try again after a while');
				}
				$('[type="password"]').val('');
			});
}

function submitForgotPassword(obj) {
	var form = $(obj).ajaxSubmit();
	var xhr = form.data('jqxhr');
	xhr.done(function(d, s, r) {
		window.location.href = window.location.origin
				+ '/forgot-password-successful';
	});
	xhr.fail(function(x) {
		window.location.href = window.location.origin
				+ '/forgot-password-successful';
	});
}